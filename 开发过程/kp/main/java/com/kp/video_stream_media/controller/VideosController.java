package com.kp.video_stream_media.controller;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.kp.common.log.annotation.Log;
import com.kp.common.log.enums.BusinessType;
import com.kp.common.security.annotation.RequiresPermissions;
import com.kp.video_stream_media.domain.Videos;
import com.kp.video_stream_media.service.IVideosService;
import com.kp.common.core.web.controller.BaseController;
import com.kp.common.core.web.domain.AjaxResult;
import com.kp.common.core.utils.poi.ExcelUtil;
import com.kp.common.core.web.page.TableDataInfo;

/**
 * 视频元数据Controller
 * 
 * @author kp
 * @date 2023-05-12
 */
@RestController
@RequestMapping("/videos")
public class VideosController extends BaseController
{
    @Autowired
    private IVideosService videosService;

    /**
     * 查询视频元数据列表
     */
    @RequiresPermissions("video_stream_media:videos:list")
    @GetMapping("/list")
    public TableDataInfo list(Videos videos)
    {
        startPage();
        List<Videos> list = videosService.selectVideosList(videos);
        return getDataTable(list);
    }

    /**
     * 导出视频元数据列表
     */
    @RequiresPermissions("video_stream_media:videos:export")
    @Log(title = "视频元数据", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, Videos videos)
    {
        List<Videos> list = videosService.selectVideosList(videos);
        ExcelUtil<Videos> util = new ExcelUtil<Videos>(Videos.class);
        util.exportExcel(response, list, "视频元数据数据");
    }

    /**
     * 获取视频元数据详细信息
     */
    @RequiresPermissions("video_stream_media:videos:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(videosService.selectVideosById(id));
    }

    /**
     * 新增视频元数据
     */
    @RequiresPermissions("video_stream_media:videos:add")
    @Log(title = "视频元数据", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody Videos videos)
    {
        return toAjax(videosService.insertVideos(videos));
    }

    /**
     * 修改视频元数据
     */
    @RequiresPermissions("video_stream_media:videos:edit")
    @Log(title = "视频元数据", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody Videos videos)
    {
        return toAjax(videosService.updateVideos(videos));
    }

    /**
     * 删除视频元数据
     */
    @RequiresPermissions("video_stream_media:videos:remove")
    @Log(title = "视频元数据", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(videosService.deleteVideosByIds(ids));
    }
}
