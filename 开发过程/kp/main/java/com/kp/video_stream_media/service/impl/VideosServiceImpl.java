package com.kp.video_stream_media.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.kp.video_stream_media.mapper.VideosMapper;
import com.kp.video_stream_media.domain.Videos;
import com.kp.video_stream_media.service.IVideosService;

/**
 * 视频元数据Service业务层处理
 * 
 * @author kp
 * @date 2023-05-12
 */
@Service
public class VideosServiceImpl implements IVideosService 
{
    @Autowired
    private VideosMapper videosMapper;

    /**
     * 查询视频元数据
     * 
     * @param id 视频元数据主键
     * @return 视频元数据
     */
    @Override
    public Videos selectVideosById(Long id)
    {
        return videosMapper.selectVideosById(id);
    }

    /**
     * 查询视频元数据列表
     * 
     * @param videos 视频元数据
     * @return 视频元数据
     */
    @Override
    public List<Videos> selectVideosList(Videos videos)
    {
        return videosMapper.selectVideosList(videos);
    }

    /**
     * 新增视频元数据
     * 
     * @param videos 视频元数据
     * @return 结果
     */
    @Override
    public int insertVideos(Videos videos)
    {
        return videosMapper.insertVideos(videos);
    }

    /**
     * 修改视频元数据
     * 
     * @param videos 视频元数据
     * @return 结果
     */
    @Override
    public int updateVideos(Videos videos)
    {
        return videosMapper.updateVideos(videos);
    }

    /**
     * 批量删除视频元数据
     * 
     * @param ids 需要删除的视频元数据主键
     * @return 结果
     */
    @Override
    public int deleteVideosByIds(Long[] ids)
    {
        return videosMapper.deleteVideosByIds(ids);
    }

    /**
     * 删除视频元数据信息
     * 
     * @param id 视频元数据主键
     * @return 结果
     */
    @Override
    public int deleteVideosById(Long id)
    {
        return videosMapper.deleteVideosById(id);
    }
}
