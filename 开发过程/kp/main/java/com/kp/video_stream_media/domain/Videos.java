package com.kp.video_stream_media.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.kp.common.core.annotation.Excel;
import com.kp.common.core.web.domain.BaseEntity;

/**
 * 视频元数据对象 videos
 * 
 * @author kp
 * @date 2023-05-12
 */
public class Videos extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 视频ID，自增长 */
    private Long id;

    /** 视频标题，不能为空 */
    @Excel(name = "视频标题，不能为空")
    private String title;

    /** 视频描述，不能为空 */
    @Excel(name = "视频描述，不能为空")
    private String description;

    /** 视频时长，单位秒，不能为空 */
    @Excel(name = "视频时长，单位秒，不能为空")
    private Long duration;

    /** 视频封面图URL，不能为空 */
    @Excel(name = "视频封面图URL，不能为空")
    private String cover;

    /** 视频上传时间，不能为空 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "视频上传时间，不能为空", width = 30, dateFormat = "yyyy-MM-dd")
    private Date uploadTime;

    /** 视频作者，不能为空 */
    @Excel(name = "视频作者，不能为空")
    private String author;

    /** 视频观看数，默认值为0 */
    @Excel(name = "视频观看数，默认值为0")
    private Long views;

    /** 创建时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "创建时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date createdAt;

    /** 最后更新时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "最后更新时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date updatedAt;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setTitle(String title) 
    {
        this.title = title;
    }

    public String getTitle() 
    {
        return title;
    }
    public void setDescription(String description) 
    {
        this.description = description;
    }

    public String getDescription() 
    {
        return description;
    }
    public void setDuration(Long duration) 
    {
        this.duration = duration;
    }

    public Long getDuration() 
    {
        return duration;
    }
    public void setCover(String cover) 
    {
        this.cover = cover;
    }

    public String getCover() 
    {
        return cover;
    }
    public void setUploadTime(Date uploadTime) 
    {
        this.uploadTime = uploadTime;
    }

    public Date getUploadTime() 
    {
        return uploadTime;
    }
    public void setAuthor(String author) 
    {
        this.author = author;
    }

    public String getAuthor() 
    {
        return author;
    }
    public void setViews(Long views) 
    {
        this.views = views;
    }

    public Long getViews() 
    {
        return views;
    }
    public void setCreatedAt(Date createdAt) 
    {
        this.createdAt = createdAt;
    }

    public Date getCreatedAt() 
    {
        return createdAt;
    }
    public void setUpdatedAt(Date updatedAt) 
    {
        this.updatedAt = updatedAt;
    }

    public Date getUpdatedAt() 
    {
        return updatedAt;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("title", getTitle())
            .append("description", getDescription())
            .append("duration", getDuration())
            .append("cover", getCover())
            .append("uploadTime", getUploadTime())
            .append("author", getAuthor())
            .append("views", getViews())
            .append("createdAt", getCreatedAt())
            .append("updatedAt", getUpdatedAt())
            .toString();
    }
}
