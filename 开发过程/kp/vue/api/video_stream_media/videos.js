import request from '@/utils/request'

// 查询视频元数据列表
export function listVideos(query) {
  return request({
    url: '/video_stream_media/videos/list',
    method: 'get',
    params: query
  })
}

// 查询视频元数据详细
export function getVideos(id) {
  return request({
    url: '/video_stream_media/videos/' + id,
    method: 'get'
  })
}

// 新增视频元数据
export function addVideos(data) {
  return request({
    url: '/video_stream_media/videos',
    method: 'post',
    data: data
  })
}

// 修改视频元数据
export function updateVideos(data) {
  return request({
    url: '/video_stream_media/videos',
    method: 'put',
    data: data
  })
}

// 删除视频元数据
export function delVideos(id) {
  return request({
    url: '/video_stream_media/videos/' + id,
    method: 'delete'
  })
}
