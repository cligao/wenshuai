<?php
// +----------------------------------------------------------------------
// | 本插件基于GPL-3.0开源协议
// +----------------------------------------------------------------------
// | Copyright (c) 2020~2021 https://www.lianshoulab.com/ All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( https://www.gnu.org/licenses/gpl-3.0.html )
// +----------------------------------------------------------------------
// | Author: liutu <17053613@qq.com>
// +----------------------------------------------------------------------
namespace app\api\controller\timetable;

use app\admin\model\timetable\Config;
use app\common\controller\Api;
use app\common\library\AES;

class Base extends Api
{
    protected $noNeedLogin = ['list'];
    protected $noNeedRight = ['*'];
    protected $site_id = 0;

    public function _initialize()
    {
        parent::_initialize();
    }
}