<?php

return [
    'Lid'                 => '信息id',
    'Fid'                 => '发布者id',
    'Tid'                 => '被关怀者id',
    'Starttime'           => '开始时间',
    'Endtime'             => '结束时间',
    'Love_sort'           => '类型',
    'Contents'            => '内容',
    'Love_sort 0'         => '文字',
    'Love_sort 1'         => '日课表背景',
    'Love_sort 2'         => '周课表背景',
];
