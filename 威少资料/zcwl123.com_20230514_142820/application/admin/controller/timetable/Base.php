<?php

namespace app\admin\controller\timetable;

use app\common\controller\Backend;

/**
 * 系统配置
 *
 * @icon fa fa-gears
 */
class Base extends Backend
{
    protected $model = null;
    protected $noNeedRight = ['*'];
    protected $site_id = 0;

    public function _initialize()
    {
        parent::_initialize();
    }
}