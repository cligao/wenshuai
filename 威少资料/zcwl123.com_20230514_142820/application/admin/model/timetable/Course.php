<?php

namespace app\admin\model\timetable;

use think\Model;


class Course extends Model
{

    

    

    // 表名
    protected $name = 'timetable_course';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = false;

    // 定义时间戳字段名
    protected $createTime = false;
    protected $updateTime = false;
    protected $deleteTime = false;

    // 追加属性
    protected $append = [
        'ctype_text'
    ];
    

    
    public function getCtypeList()
    {
        return ['0' => __('Ctype 0'), '1' => __('Ctype 1'), '2' => __('Ctype 2'), '3' => __('Ctype 3'), '4' => __('Ctype 4')];
    }


    public function getCtypeTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['ctype']) ? $data['ctype'] : '');
        $list = $this->getCtypeList();
        return isset($list[$value]) ? $list[$value] : '';
    }




    public function timetablesubjects()
    {
        return $this->belongsTo('app\admin\model\timetable\Subjects', 'subject_id', 'subject_id', [], 'LEFT')->setEagerlyType(0)->bind('sname');
    }
}
