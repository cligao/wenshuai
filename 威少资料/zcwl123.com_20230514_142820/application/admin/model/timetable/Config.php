<?php

namespace app\admin\model\timetable;

use think\Cache;
use think\Model;


class Config extends Model
{

    // 表名
    protected $name = 'timetable_config';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = false;

    // 定义时间戳字段名
    protected $createTime = false;
    protected $updateTime = false;
    protected $deleteTime = false;

    // 追加属性
    protected $append = [

    ];

    public static function config_info($site_id)
    {
        return get_addon_config('timetable');
    }

    public static function get_secret($site_id){
        $config = get_addon_config('timetable');
        return $config['appsecret'];
    }
}
