<?php 
 return [
    'table_name' => 'fa_timetable_config,fa_timetable_course,fa_timetable_lovers,fa_timetable_subjects,fa_timetable_usercourse,fa_timetable_usersetting',
    'self_path' => 'application/admin/lang/zh-cn/timetable'."\r\n"
        .'application/admin/controller/timetable'."\r\n"
        .'application/api/controller/timetable'."\r\n"
        .'application/admin/model/timetable'."\r\n"
        .'application/admin/view/timetable'."\r\n"
        .'public/assets/js/backend/timetable',
    'update_data' => '',
];