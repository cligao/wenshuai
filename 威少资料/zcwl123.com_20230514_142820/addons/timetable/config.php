<?php

return [
    [
        'name' => 'wxappid',
        'title' => '小程序AppID',
        'type' => 'string',
        'content' => [],
        'value' => '',
        'rule' => '',
        'msg' => '',
        'tip' => '',
        'ok' => '',
        'extend' => '',
    ],
    [
        'name' => 'wxappsecret',
        'title' => '小程序AppSecret',
        'type' => 'string',
        'content' => [],
        'value' => '',
        'rule' => '',
        'msg' => '',
        'tip' => '',
        'ok' => '',
        'extend' => '',
    ],
    [
        'name' => 'appsecret',
        'title' => '练手Lab秘钥',
        'type' => 'string',
        'content' => [],
        'value' => 'ca6dbbe3333a079b',
        'rule' => '',
        'msg' => '',
        'tip' => '16位字符串，请自己生成或到练手Lab小程序后台获取',
        'ok' => '',
        'extend' => '',
    ],
    [
        'name' => 'site_id',
        'title' => '站点id',
        'type' => 'string',
        'content' => [],
        'value' => '0',
        'rule' => '',
        'msg' => '',
        'tip' => '',
        'ok' => '',
        'extend' => '',
    ],
    [
        'name' => 'school_name',
        'title' => '学校名称',
        'type' => 'string',
        'content' => [],
        'value' => '北京大学',
        'rule' => '',
        'msg' => '',
        'tip' => '',
        'ok' => '',
        'extend' => '',
    ],
    [
        'name' => 'first_term_start',
        'title' => '第一学期开校时间',
        'type' => 'string',
        'content' => [],
        'value' => '9-7',
        'rule' => '',
        'msg' => '',
        'tip' => '格式为：月份-日期，如9-7为9月7日',
        'ok' => '',
        'extend' => '',
    ],
    [
        'name' => 'second_term_start',
        'title' => '第二学期开校时间',
        'type' => 'string',
        'content' => [],
        'value' => '3-1',
        'rule' => '',
        'msg' => '',
        'tip' => '格式为：月份-日期，如3-1为3月1日',
        'ok' => '',
        'extend' => '',
    ],
    [
        'name' => 'summer_start',
        'title' => '夏令时开始日期',
        'type' => 'string',
        'content' => [],
        'value' => '5.1',
        'rule' => '',
        'msg' => '',
        'tip' => '格式为：月份.日期，如5.1为5月1日',
        'ok' => '',
        'extend' => '',
    ],
    [
        'name' => 'summer_hours',
        'title' => '夏令时上课时间',
        'type' => 'text',
        'content' => [],
        'value' => '[{"s_time":"8:00","e_time":"8:45"},{"s_time":"8:55","e_time":"9:40"},{"s_time":"10:00","e_time":"10:45"},{"s_time":"10:55","e_time":"11:40"},{"s_time":"14:00","e_time":"14:45"},{"s_time":"14:55","e_time":"15:40"},{"s_time":"16:00","e_time":"16:45"},{"s_time":"16:55","e_time":"17:40"},{"s_time":"18:55","e_time":"19:40"},{"s_time":"19:55","e_time":"20:40"}]',
        'rule' => '',
        'msg' => '',
        'tip' => '',
        'ok' => '',
        'extend' => '',
    ],
    [
        'name' => 'winter_start',
        'title' => '冬令时开始日期',
        'type' => 'string',
        'content' => [],
        'value' => '11.1',
        'rule' => '',
        'msg' => '',
        'tip' => '格式为：月份.日期，如11.1为11月1日',
        'ok' => '',
        'extend' => '',
    ],
    [
        'name' => 'winter_hours',
        'title' => '冬令时上课时间',
        'type' => 'text',
        'content' => [],
        'value' => '[{"s_time":"8:00","e_time":"8:45"},{"s_time":"8:55","e_time":"9:40"},{"s_time":"10:00","e_time":"10:45"},{"s_time":"10:55","e_time":"11:40"},{"s_time":"14:00","e_time":"14:45"},{"s_time":"14:55","e_time":"15:40"},{"s_time":"16:00","e_time":"16:45"},{"s_time":"16:55","e_time":"17:40"},{"s_time":"18:55","e_time":"19:40"},{"s_time":"19:55","e_time":"20:40"}]',
        'rule' => '',
        'msg' => '',
        'tip' => '',
        'ok' => '',
        'extend' => '',
    ],
];
