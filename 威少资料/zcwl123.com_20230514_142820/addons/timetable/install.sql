CREATE TABLE IF NOT EXISTS `__PREFIX__timetable_config` (
  `site_id` int(11) NOT NULL AUTO_INCREMENT,
  `school_name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '学校名称',
  `first_term_start` varchar(5) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '9-7' COMMENT '第一学期开校时间',
  `second_term_start` varchar(5) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '3-1' COMMENT '第二学期开校时间',
  `summer_start` varchar(5) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '5.1' COMMENT '夏令时开始日期',
  `summer_hours` varchar(1000) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '[{"s_time":"8:00","e_time":"8:45"},{"s_time":"8:55","e_time":"9:40"},{"s_time":"10:00","e_time":"10:45"},{"s_time":"10:55","e_time":"11:40"},{"s_time":"14:00","e_time":"14:45"},{"s_time":"14:55","e_time":"15:40"},{"s_time":"16:00","e_time":"16:45"},{"s_time":"16:55","e_time":"17:40"},{"s_time":"18:55","e_time":"19:40"},{"s_time":"19:55","e_time":"20:40"}]' COMMENT '夏令时上课时间',
  `winter_start` varchar(5) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '11.1' COMMENT '冬令时开始日期',
  `winter_hours` varchar(1000) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '[{"s_time":"8:00","e_time":"8:45"},{"s_time":"8:55","e_time":"9:40"},{"s_time":"10:00","e_time":"10:45"},{"s_time":"10:55","e_time":"11:40"},{"s_time":"14:00","e_time":"14:45"},{"s_time":"14:55","e_time":"15:40"},{"s_time":"16:00","e_time":"16:45"},{"s_time":"16:55","e_time":"17:40"},{"s_time":"18:55","e_time":"19:40"},{"s_time":"19:55","e_time":"20:40"}]' COMMENT '冬令时上课时间',
  `appid` int(11) DEFAULT NULL COMMENT '练手Lab appid',
  `appsecret` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '练手Lab秘钥',
  `wxappid` varchar(18) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '微信小程序的appid',
  `wxappsecret` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '微信小程序的appsecret',
  PRIMARY KEY (`site_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='配置表';

CREATE TABLE IF NOT EXISTS `__PREFIX__timetable_course` (
  `ciid` bigint(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '排课表',
  `subject_id` int(11) NOT NULL COMMENT '课目id',
  `classroom` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '教室号',
  `days` tinyint(1) NOT NULL COMMENT '周几',
  `nums` tinyint(2) NOT NULL COMMENT '第几节课开始',
  `enum` tinyint(2) NOT NULL COMMENT '结束节数',
  `attend` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '[]' COMMENT '上课周数',
  `ctype` enum('0','1','2','3','4') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0' COMMENT '课程类型（0代表普通课，1代表实验课，2代表网课，3代表考试，4代表其他）',
  `tname` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '老师姓名',
  PRIMARY KEY (`ciid`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='用户课程详情表';

CREATE TABLE IF NOT EXISTS `__PREFIX__timetable_lovers` (
  `lid` bigint(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '关怀id',
  `fid` bigint(11) unsigned NOT NULL COMMENT '发布者id',
  `tid` bigint(11) unsigned NOT NULL COMMENT '被关怀者id',
  `starttime` int(11) NOT NULL COMMENT '开始时间',
  `endtime` int(11) NOT NULL COMMENT '结束时间',
  `love_sort` enum('0','1','2') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0' COMMENT '0为文字，1为日课表背景，2为周课表背景',
  `contents` varchar(600) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '内容',
  PRIMARY KEY (`lid`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='情侣留言表';

CREATE TABLE IF NOT EXISTS `__PREFIX__timetable_subjects` (
  `subject_id` bigint(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '课目id',
  `status` enum('0','1','2') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0' COMMENT '科目类型，0表示线下课，1表示线上课，2表示实践课',
  `sname` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '课目名称',
  `credit` float(10,1) DEFAULT '0.0' COMMENT '学分',
  PRIMARY KEY (`subject_id`) USING BTREE,
  UNIQUE KEY `courseid` (`subject_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='科目表';

CREATE TABLE IF NOT EXISTS `__PREFIX__timetable_usercourse` (
  `usercourse_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL COMMENT '用户id',
  `ciid` int(11) NOT NULL COMMENT '排课表id',
  PRIMARY KEY (`usercourse_id`),
  UNIQUE KEY `usercourse_id` (`usercourse_id`),
  KEY `usercourse_id_2` (`usercourse_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='课程表';

CREATE TABLE IF NOT EXISTS `__PREFIX__timetable_usersetting` (
  `user_id` bigint(11) NOT NULL COMMENT '关联用户id',
  `entry_year` int(4) NOT NULL DEFAULT '0' COMMENT '入学年份',
  `lovers_id` bigint(11) DEFAULT NULL COMMENT '默认显示的情侣id',
  `has_apply` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0' COMMENT '0表示没申请，1表示有申请',
  `weeksday` tinyint(1) NOT NULL DEFAULT '0' COMMENT '每周几天，0表示5天，1表示6天，2表示7天',
  `table_bgimage` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '课程表页面背景图片',
  `index_bgimage` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '首页背景图片',
  `id_show_lover` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0' COMMENT '是否显示情侣关怀，0表示显示，1表示拒绝',
  `my_course_info` varchar(5000) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '课程信息（ciid及详细信息）',
  `lover_course_info` varchar(5000) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '情侣课程信息',
  `login_num` int(11) NOT NULL DEFAULT '0' COMMENT '今日登录次数',
  `last_login_time` int(11) NOT NULL DEFAULT '0' COMMENT '最后登录时间',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='用户扩展信息表';

