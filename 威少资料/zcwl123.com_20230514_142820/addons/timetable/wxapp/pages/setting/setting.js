/* eslint-disable no-undef */
// +----------------------------------------------------------------------
// | 本插件基于GPL-3.0开源协议
// +----------------------------------------------------------------------
// | Copyright (c) 2020~2021 https://www.lianshoulab.com/ All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( https://www.gnu.org/licenses/gpl-3.0.html )
// +----------------------------------------------------------------------
// | Author: liutu <17053613@qq.com>
// +----------------------------------------------------------------------
//index.js
import config from "../../config";
import { userEdit } from "../../utils/api/user";
import { wxShowToast } from "../../utils/promisify";

//获取应用实例
const app = getApp();

Page({
    data: {
        StatusBar: app.globalData.StatusBar,
        CustomBar: app.globalData.CustomBar,
        ImgUrl: app.globalData.ImgUrl,
        displayArea: app.globalData.displayArea,
        avatar: null,
        nickname: null,
        gender: null,
        grade: null,
        gradeList: config.gradeList,
        modal: null,
        nicknameEdit: "",
    },
    onLoad: function () {
        this.setData({
            avatar: app.globalData.userInfo.avatar,
            nickname: app.globalData.userInfo.nickname,
            gender: app.globalData.userInfo.gender,
            grade: app.globalData.userInfo.grade,
        });
    },
    /**
     * 后退一页
     */
    BackPage() {
        wx.navigateBack({
            delta: 1,
        });
    },
    /**
     * 注销用户
     */
    // logOut() {
    //     wx.removeStorage({
    //         key: "token",
    //         success(res) {
    //             console.log(res);
    //         },
    //     });
    //     app.globalData.userInfo = null;
    // },
    /**
     * 更换头像
     */
    //   replacePicture() {
    //     wx.chooseImage({
    //       count: 1,
    //       sizeType: ["original", "compressed"],
    //       sourceType: ["album", "camera"],
    //       success: (res) => {
    //         // tempFilePath可以作为img标签的src属性显示图片
    //         const tempFilePaths = res.tempFilePaths;
    //         this.setData({
    //           avatar: tempFilePaths[0],
    //         });
    //       },
    //     });
    //   },
    /**
     * 显示弹窗
     */
    showModal(e) {
        let { modal } = e.currentTarget.dataset;
        if (modal === "nickname") {
            this.setData({
                nicknameEdit: this.data.nickname,
            });
        }
        this.setData({
            modal,
        });
    },
    /**
     * 关闭弹窗
     */
    cancelModal() {
        this.setData({
            modal: false,
            nicknameEdit: "",
        });
    },
    /**
     * 昵称修改
     */
    modifyNickname(e) {
        let value = e.detail.value;
        console.log(value);
        this.setData({
            nicknameEdit: value,
        });
    },
    /**
     * 修改用户信息
     */
    modifyUserInfo() {
        let { nickname, gender, grade } = this.data;
        userEdit({
            nickname,
            gender,
            grade,
        }).then((v) => {
            console.log(v);
            v.code && wxShowToast(v.msg);
            app.getSet();
        });
    },
    /**
     * 昵称修改确认
     */
    nicknameConfirm() {
        let { nicknameEdit } = this.data;
        this.setData({
            nickname: nicknameEdit,
        });
        this.modifyUserInfo();
        this.cancelModal();
    },
    /**
     * 性别更改
     */
    modifyGender(e) {
        let gender = e.currentTarget.dataset.id;
        this.setData({
            gender,
        });
        this.modifyUserInfo();
        this.cancelModal();
    },
    /**
     * 年级更改
     */
    modifyGrade(e) {
        let grade = e.currentTarget.dataset.id;
        this.setData({
            grade,
        });
        this.modifyUserInfo();
        this.cancelModal();
    },
});
