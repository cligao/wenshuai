/* eslint-disable no-undef */
// +----------------------------------------------------------------------
// | 本插件基于GPL-3.0开源协议
// +----------------------------------------------------------------------
// | Copyright (c) 2020~2021 https://www.lianshoulab.com/ All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( https://www.gnu.org/licenses/gpl-3.0.html )
// +----------------------------------------------------------------------
// | Author: liutu <17053613@qq.com>
// +----------------------------------------------------------------------
//index.js
import config from "../../config";
import dayjs from "../../utils/dayjs/dayjs.min";
import { serializePathQuery } from "../../utils/api/http";
//获取应用实例
const app = getApp();

Page({
    data: {
        StatusBar: app.globalData.StatusBar,
        CustomBar: app.globalData.CustomBar,
        displayArea: app.globalData.displayArea,
        ImgUrl: app.globalData.ImgUrl,
        colorList: config.colorList,
        gradeList: config.gradeList,
        weeksSwitch: false,
        semester: null,
        haveCouple: null, // 情侣ID,用于判断是否绑定情侣
        showWeek: 8, // 显示周
        currentWeek: 8, // 当前周
        weeksList: [], // 当前周日期列表
        showcouples: false, // 情侣课表切换
        editTableId: null,
        classTime: null,
        courseInfo: [], // 自己课表
        loverCourse: [], // 情侣课表
        checkbox: [],
    },
    // 上课节数
    courseNum() {
        let checkbox = [];
        for (let index = 1; index <= 20; index++) {
            checkbox.push({
                value: index,
                checked: false,
            });
        }
        this.setData({
            checkbox,
        });
    },
    /**
     * 生成当前周的日期列表
     */
    weeksList(day) {
        let todayWeek = day.format("d") ? dayjs().format("d") - 1 : 6;
        let Fist = day.subtract(todayWeek, "day");
        let _week = [];
        for (let index = 0; index < 7; index++) {
            _week.push(Fist.add(index, "day").format("MM/DD"));
        }
        this.setData({
            weeksList: _week,
            todayWeek,
        });
    },
    /**
     * 计算课程表是否符合当前周
     * @param {*} week 显示周
     * @param {*} userInfo 数据
     */
    weekCourseInfo(week, userInfo) {
        if (!userInfo) {
            return null;
        } else {
            let courseInfo = userInfo
                .map((v) => {
                    let attend = v.attend.split(",").filter((e) => e == week);
                    if (attend[0]) {
                        return v;
                    }
                })
                .filter((v) => v);
            return courseInfo.length > 0 ? courseInfo : null;
        }
    },
    /**
     * 设置课程表
     */
    setcourse(week, userInfo = app.globalData.userInfo) {
        // 是否有情侣
        let haveCouple =
            userInfo && userInfo.lovers_id ? userInfo.lovers_id : null;
        // 本人课表
        let courseInfo =
            userInfo && userInfo.my_course_info
                ? JSON.parse(userInfo.my_course_info)
                : null;
        // 情侣课表
        let loverCourse =
            userInfo && userInfo.lover_course_info
                ? JSON.parse(userInfo.lover_course_info)
                : null;
        courseInfo = this.weekCourseInfo(week, courseInfo);
        loverCourse = this.weekCourseInfo(week, loverCourse);
        this.setData({
            userInfo,
            courseInfo,
            loverCourse,
            haveCouple,
        });
    },
    onShow: function () {
        let currentWeek = wx.getStorageSync("currentWeek");
        // 判断是否有内容更改
        if (JSON.stringify(this.data.userInfo) != app.globalData.userInfo) {
            this.setcourse(currentWeek);
        }
        if (!this.data.classTime) {
            let classTime = wx.getStorageSync("classTime");
            this.setData({
                classTime,
                currentWeek,
                showWeek: currentWeek,
            });
        }
    },
    onLoad: function () {
        this.courseNum();
        this.weeksList(dayjs());
        let semester = null; // 当前是第几学期
        let nowM = dayjs().format("M"); // 获取现在的月份
        semester = nowM < 3 || nowM > 9 ? 1 : 2;
        this.setData({
            semester,
        });
        let currentWeek = wx.getStorageSync("currentWeek");
        if (app.globalData.userInfo) {
            this.setcourse(currentWeek);
        }
    },
    /**
     * 回到当前周
     */
    backToCurrent() {
        this.setData({
            showWeek: this.data.currentWeek,
            weeksSwitch: false,
        });
        this.weeksList(dayjs());
        let currentWeek = wx.getStorageSync("currentWeek");
        this.setcourse(currentWeek);
    },
    /**
     * 选择周
     */
    ChooseCheckbox(e) {
        if (!app.globalData.userInfo) {
            return;
        }
        let value = e.currentTarget.dataset.value;
        let diff = value - this.data.currentWeek;
        this.setData({
            showWeek: value,
        });
        this.setcourse(value);
        if (diff == 0) {
            // 没有周数切换
            return;
        } else if (diff > 0) {
            let _add = dayjs().add(diff, "week");
            this.weeksList(_add);
        } else {
            let _sub = dayjs().subtract(-diff, "week");
            this.weeksList(_sub);
        }
    },
    /**
     * 周数切换
     */
    weeksSwitch() {
        this.setData({
            weeksSwitch: true,
        });
    },
    hideweeksSwitch() {
        this.setData({
            weeksSwitch: false,
        });
    },
    /**
     * 情侣课表切换
     */
    switchCouples() {
        if (!app.globalData.userInfo) {
            return;
        }
        if (this.data.haveCouple) {
            let showcouples = !this.data.showcouples;
            this.setData({
                showcouples,
            });
        } else {
            // 如果没有绑定情侣,则弹窗让其跳转到绑定情侣页面
            wx.showModal({
                title: "温馨提示",
                content: "您还没有绑定情侣哦,是否跳转到绑定页面?",
                success(res) {
                    if (res.confirm) {
                        wx.navigateTo({
                            url: "/pages/couples/couples?",
                        });
                    } else if (res.cancel) {
                        return;
                    }
                },
            });
        }
    },
    /**
     * 新增课表
     */
    addTable(e) {
        let id = e.detail;
        wx.navigateTo({
            url:
                "/pages/editingCourse/editingCourse?" +
                serializePathQuery({
                    addTime: [parseInt(id % 7), parseInt(id / 7)],
                }),
        });
    },
    /**
     * 编辑课表
     */
    editTable(e) {
        wx.navigateTo({
            url:
                "/pages/editingCourse/editingCourse?" +
                serializePathQuery({
                    id: e.detail,
                }),
        });
    },
});
