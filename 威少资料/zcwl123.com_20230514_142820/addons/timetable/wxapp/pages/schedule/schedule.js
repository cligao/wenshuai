/* eslint-disable no-undef */
// +----------------------------------------------------------------------
// | 本插件基于GPL-3.0开源协议
// +----------------------------------------------------------------------
// | Copyright (c) 2020~2021 https://www.lianshoulab.com/ All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( https://www.gnu.org/licenses/gpl-3.0.html )
// +----------------------------------------------------------------------
// | Author: liutu <17053613@qq.com>
// +----------------------------------------------------------------------
import {
    scheduleBG,
    couplesBG,
    couplesInfoAdd,
    setbgdefault,
} from "../../utils/api/user";
//获取应用实例
const app = getApp();
const dayjs = require("../../utils/dayjs/dayjs.min");
import { wxShowToast } from "../../utils/promisify";
Page({
    data: {
        StatusBar: app.globalData.StatusBar,
        CustomBar: app.globalData.CustomBar,
        ImgUrl: app.globalData.ImgUrl,
        displayArea: app.globalData.displayArea,
        CouplesBG: null, // 情侣课表背景
        dailySchedule: null, // 日课表背景
        weeklySchedule: null, // 周课表背景
        sendTime: "现在", // 情侣课表背景开始时间
        displayTimeList: [
            {
                label: "一天",
                checked: false,
                time: 1,
            },
            {
                label: "一周",
                checked: true,
                time: 7,
            },
            {
                label: "一个月",
                checked: false,
                time: 30,
            },
        ],
        startTime: null,
    },
    onLoad: function (query) {
        let { index_bgimage, table_bgimage } = app.globalData.userInfo;
        this.setData({
            couples: query.couples ? query.couples : null,
            dailySchedule: index_bgimage
                ? app.globalData.ImgUrl + index_bgimage
                : null, // 日课表背景
            weeklySchedule: table_bgimage
                ? app.globalData.ImgUrl + table_bgimage
                : null, // 周课表背景
        });
    },
    /**
     * 后退一页
     */
    BackPage() {
        wx.navigateBack({
            delta: 1,
        });
    },
    /**
     * 切换展示时间
     */
    showTime(e) {
        let id = e.currentTarget.dataset.id;
        console.log(e.currentTarget.dataset.id);
        let displayTimeList = this.data.displayTimeList.map((v, i) => {
            v.checked = id === i;
            return v;
        });
        this.setData({
            displayTimeList,
        });
    },
    /**
     * 修改背景
     */
    modifyImage(e) {
        let type = e.currentTarget.dataset.type;
        wx.chooseImage({
            count: 1,
            sizeType: ["compressed"],
            sourceType: ["album"],
            success: (res) => {
                // tempFilePath可以作为img标签的src属性显示图片
                const tempFilePaths = res.tempFilePaths;
                switch (type) {
                case "week":
                    scheduleBG(tempFilePaths[0], "table").then((v) => {
                        app.getSet();
                        v.code && wxShowToast(v.msg);
                        this.setData({
                            weeklySchedule: tempFilePaths[0],
                        });
                    });
                    break;
                case "daily":
                    scheduleBG(tempFilePaths[0], "index").then((v) => {
                        app.getSet();
                        v.code && wxShowToast(v.msg);
                        this.setData({
                            dailySchedule: tempFilePaths[0],
                        });
                    });
                    break;
                case "Couples":
                    couplesBG(tempFilePaths[0]).then((v) => {
                        console.log(v);
                        app.getSet();
                        v.code && wxShowToast(v.msg);
                        this.setData({
                            CouplesBG:
                                    app.globalData.ImgUrl + v.data.imgurl,
                        });
                    });
                    break;
                default:
                    wxShowToast("设置失败,请重试");
                    break;
                }
            },
        });
    },
    /**
     * 恢复默认背景
     */
    saveButton() {
        this.setData({
            dailySchedule: null,
            weeklySchedule: null,
        });
        setbgdefault().then((v) => {
            v.code && wxShowToast(v.msg);
            app.getSet();
        });
    },
    /**
     * 情侣课表开始时间
     */
    TimePicker(e) {
        let day = dayjs(e.detail).format("MM月DD日");
        let am = dayjs(e.detail).format("A") == "PM" ? "下午" : "上午";
        let time = dayjs(e.detail).format("HH:mm");
        let sendTime = `${day} ${am} ${time}`;
        console.log(sendTime, "TimePicker");
        this.setData({
            sendTime,
            startTime: dayjs(e.detail),
        });
    },
    /**
     * 保存情侣背景
     */
    saveCouplesBG() {
        let { displayTimeList, startTime, CouplesBG } = this.data;
        let endType = displayTimeList.filter((v) => v.checked === true)[0];
        let _startTime = startTime ? dayjs(startTime) : dayjs();
        let endTime = endType.time;
        let tid = app.globalData.userInfo.lovers_id;
        if (!tid) {
            wx.showToast({
                title: "保存失败",
                icon: "none",
                duration: 2000,
            });
            return;
        }
        couplesInfoAdd({
            tid,
            starttime: _startTime.unix(),
            endtime: endTime,
            love_sort: 1,
            contents: CouplesBG.replace(this.data.ImgUrl, ""),
        }).then((v) => {
            wxShowToast(v.msg);
            if (v.code) {
                app.getSet().then(() => {
                    this.BackPage();
                });
            }
        });
    },
});
