/* eslint-disable no-undef */
const dateTimePicker = require("../../utils/dateTimePicker");

Component({
    /**
     * 组件的一些选项
     */
    options: {
        addGlobalClass: true,
        multipleSlots: true,
    },
    /**
     * 组件的对外属性
     */
    properties: {
        type: {
            type: String,
            default: "minute",
        },
    },
    /**
     * 组件的初始数据
     */
    data: {
        date: "2018-03-15",
        time: "12:01",
        dateTimeSecondArray: null,
        dateTimeSecond: null,
        dateTimeMinuteArray: null,
        dateTimeMinute: null,
        startYear: 2000,
        endYear: 2050,
    },
    // 生命周期函数，可以为函数，或一个在methods段中定义的方法名
    attached: function () {
        let obj1 = dateTimePicker.dateTimePicker(
            this.data.startYear,
            this.data.endYear
        );
        let obj2 = dateTimePicker.dateTimePicker(
            this.data.startYear,
            this.data.endYear
        );
        // 精确到分的处理，将数组的秒去掉
        // let lastArray = obj2.dateTimeArray.pop();
        // let lastTime = obj2.dateTime.pop();
        console.log(obj1.dateTime);
        this.setData({
            dateTimeSecond: obj1.dateTime,
            dateTimeSecondArray: obj1.dateTimeArray,
            dateTimeMinute: obj2.dateTime,
            dateTimeMinuteArray: obj2.dateTimeArray,
        });
    },
    ready: function () {},
    /**
     * 组件的方法列表
     */
    methods: {
        /**
         * 格式化输出内容
         * @param {*} dateTimeMinute 选择器选择后的内容
         * @return {*} 格式化后的选项
         */
        returnValue(dateTimeMinute) {
            console.log(dateTimeMinute, "dateTimeMinute");
            let { dateTimeMinuteArray } = this.data;
            let year = dateTimeMinuteArray[0][dateTimeMinute[0]];
            let month = dateTimeMinuteArray[1][dateTimeMinute[1]];
            let day = dateTimeMinuteArray[2][dateTimeMinute[2]];
            let hour = dateTimeMinuteArray[3][dateTimeMinute[3]];
            let minute = dateTimeMinuteArray[4][dateTimeMinute[4]];
            let time = `${year}${month}${day}`;
            time = time.replace(/([年月])/g, "-");
            time = time.replace(/日/g, "");
            if (dateTimeMinute.length < 6) {
                return `${time} ${hour}:${minute}`;
            } else {
                let second = dateTimeMinuteArray[5][dateTimeMinute[5]];
                return `${time} ${hour}:${minute}:${second}`;
            }
        },
        changeDateTimeSecond(e) {
            console.log("changeDateTimeSecond: " + e.detail.value);
            this.setData({ dateTime: e.detail.value });
            let _returnValue = this.returnValue(e.detail.value);
            this.triggerEvent("change", _returnValue);
        },

        changeDateTimeMinute(e) {
            console.log("changeDateTimeMinute: " + e.detail.value);
            this.setData({ dateTime1: e.detail.value });
            let _returnValue = this.returnValue(e.detail.value);
            this.triggerEvent("change", _returnValue);
        },

        changeDateTimeSecondColumn(e) {
            var arr = this.data.dateTimeSecond,
                dateArr = this.data.dateTimeSecondArray;
            arr[e.detail.column] = e.detail.value;
            dateArr[2] = dateTimePicker.getMonthDay(
                dateArr[0][arr[0]],
                dateArr[1][arr[1]]
            );
            this.setData({
                dateTimeSecondArray: dateArr,
                dateTimeSecond: arr,
            });
        },

        changeDateTimeMinuteColumn(e) {
            var arr = this.data.dateTimeMinute,
                dateArr = this.data.dateTimeMinuteArray;
            arr[e.detail.column] = e.detail.value;
            dateArr[2] = dateTimePicker.getMonthDay(
                dateArr[0][arr[0]],
                dateArr[1][arr[1]]
            );
            this.setData({
                dateTimeMinuteArray: dateArr,
                dateTimeMinute: arr,
            });
        },
    },
});
