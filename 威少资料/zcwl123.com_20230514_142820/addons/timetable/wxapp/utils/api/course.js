import {
    getWithToken,
    postWithToken,
    showLoading,
} from "./http";

/**
 * 课程详情查询
 * @param {*} course_id 课程id
 * @return {*}
 */
export function courseInfo(params) {
    return showLoading(getWithToken("/usercourse/info", params), "查询中");
}
/**
 * 添加课程表
 * @param {*} course_id 课程id
 * @return {*}
 */
export function courseAdd(params) {
    return showLoading(postWithToken("/usercourse/add", params, true), "增加中");
}
/**
 * 删除课程信息（单节课）
 * @param {*} usercourse_id 单课程id
 * @return {*}
 */
export function courseDel(params) {
    return showLoading(postWithToken("/usercourse/del", params), "删除中");
}
/**
 * 获取添加过的科目列表
 * @param {*}
 * @return {*}
 */
export function courseNameList() {
    return getWithToken("/usercourse/courselist");
}
