<?php

namespace addons\delivery\model;

use think\Model;
/**
 * 标签模型
 */
class Express Extends Model
{
    protected $name = "delivery_express";
    // 开启自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';
    // 定义时间戳字段名
    protected $createTime = '';
    protected $updateTime = '';
    // 追加属性
    protected $append = [

    ];
    protected static $config = [];

}
