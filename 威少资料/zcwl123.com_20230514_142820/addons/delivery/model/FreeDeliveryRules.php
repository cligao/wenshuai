<?php

namespace addons\delivery\model;

use think\Model;
use traits\model\SoftDelete;
/**
 * 标签模型
 */
class FreeDeliveryRules Extends Model
{
    use SoftDelete;
    protected $name = "delivery_free_delivery_rules";
    // 开启自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';
    // 定义时间戳字段名
    protected $createTime = 'addtime';
    protected $updateTime = '';
    protected $deleteTime = 'deletetime';
    // 追加属性
    protected $append = [
        'city_name'
    ];
    protected static $config = [];
    public function getStatusList()
    {
        return ['1' => __('按重计费'), '2' => __('按件计费')];
    }
    public function getCityNameAttr($value, $data)
    {
        $model = model('addons\delivery\model\DistrictArr');
        $cityList = $model::getArr();
        $city_array=json_decode($data['city'],true);
        $str = '';
        foreach($city_array as $i=>$v){
            $str .=$cityList[$v]['name'].'&nbsp;&nbsp;';
        }
        return $str;
    }
}
