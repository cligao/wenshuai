<?php

namespace addons\delivery\model;

use app\models\FreeDeliveryRules;
use app\models\MchOption;
use think\Model;
use traits\model\SoftDelete;
/**
 * 标签模型
 */
class Order Extends Model
{
    use SoftDelete;
    protected $name = "delivery_order";
    // 开启自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';
    // 定义时间戳字段名
    protected $createTime = 'addtime';
    protected $updateTime = '';
    protected $deleteTime = 'deletetime';
    // 追加属性
    protected $append = [

    ];
    protected static $config = [];

    protected static function init()
    {
        $config = get_addon_config('delivery');
        self::$config = $config;
    }
    public function makeOrderNo(){
        $order_no = null;
        while (true) {
            $order_no = date('YmdHis') . mt_rand(100000, 999999);
            $exist_order_no = Order::where('order_no',$order_no)->field('id')->find()['id'];
            if (!$exist_order_no) {
                break;
            }
        }
        return $order_no;
    }
    public function getStatusList()
    {
        return ['0' => __('快递'), '1' => __('无需物流')];
    }
    public function orderdetail(){

        return $this->hasMany('addons\delivery\model\OrderDetail','order_id','id');

    }
    //组装商品
    public static function changeGoods($goodslist)
    {

        $order_goods=[];
        foreach($goodslist as $i=>$v){
            $temp=[];
            $temp['goods_id']=$v['goods_id'];

            $temp['num']=$v['num'];
            $temp['price']=$v['price'];
            $temp['name']=$v['name'];
            $temp['cimage']=$v['cimage'];
            $temp['freight']=$v['freight'];//运费模板
            $temp['weight']=$v['weight'];//重量
            $order_goods[$v['goods_id']]=$temp;
        }
        return   $order_goods;
    }
    //检查商品
    public static function checkGoods($params)
    {
        $order_goods=[];
        $goods_model=model('addons\delivery\model\Goods');
        foreach($params['goods']['id'] as $i=>$v){
            if(empty($params['goods']['num'][$i]) || !is_numeric($params['goods']['num'][$i])){
                return false;
            }

            //将商品id一样的商品数量合并
            if(isset($order_goods[$v])){
                $order_goods[$v]['num'] +=$params['goods']['num'][$i];
            }else{
                $temp=[];
                $temp['goods_id']=$v;

                $temp['num']=$params['goods']['num'][$i];
                $goods_info=$goods_model->get($v);
                if(empty($goods_info)){
                    return false;
                }
                $temp['price']=$goods_info['price'];
                $temp['name']=$goods_info['name'];
                $temp['cimage']=$goods_info['cimage'];
                $temp['freight']=$goods_info['freight'];//运费模板
                $temp['weight']=$goods_info['weight'];//重量
                $order_goods[$v]=$temp;
            }
        }
        return   $order_goods;
    }
    //计算邮费
    public static function getExpressPrice($address,$goodsList,$total_price)
    {

        $expressPrice = 0;
        //获取地址的城市和省份id
        $address['city_id']=0;
        $address['province_id']=0;
        $provinceList = DistrictArr::getTree(0,[]);

        foreach($provinceList as $i=>$v){
            if(strpos($v['text'],$address['accept_province']) !== false){
                $address['province_id']=$v['id'];
            }
        }
        $cityList = DistrictArr::getCityList();

        foreach($cityList as $i=>$v){
            if(strpos($v['text'],$address['accept_city']) !== false){
                $address['city_id']=$v['id'];
            }
        }

        //通过运费规则计算运费
        if(!empty($goodsList)){
            $expressPrice = PostageRules::getExpressPriceMore($address['city_id'], $goodsList, $address['province_id']);
        }else{
            $expressPrice = 0;
        }



        $expressPrice = self::getFreeDeliveryRules($address['city_id'],$total_price, $expressPrice);

        return $expressPrice;
    }
    // 包邮规则
    public static function getFreeDeliveryRules($city_id,$total_price, $expressPrice)
    {
        if ($expressPrice == 0) {
            return $expressPrice;
        }

        $free =model('addons\delivery\model\FreeDeliveryRules')->all();
        foreach ($free as $k => $v) {
            $city = json_decode($v['city'], true);
            foreach ($city as $v1) {
                if ($city_id == $v1 && $total_price >= $v['price']) {
                    $expressPrice = 0;
                    break;
                }
            }
        }

        return $expressPrice;
    }
}
