<?php

namespace addons\delivery\model;

use think\Model;
use traits\model\SoftDelete;
/**
 * 标签模型
 */
class ExpressBill Extends Model
{
    use SoftDelete;
    protected $name = "delivery_express_bill";
    // 开启自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';
    // 定义时间戳字段名
    protected $createTime = 'addtime';
    protected $updateTime = '';
    protected $deleteTime = 'deletetime';
    // 追加属性
    protected $append = [
        'express_com'
    ];
    protected static $config = [];
    public function getStatusList()
    {
        return ['1' => __('按重计费'), '2' => __('按件计费')];
    }
    public function getExpressComAttr($value, $data)
    {
        $model = model('addons\delivery\model\Express');
        $express_info = $model->get($data['express_id']);
        return $express_info['name'];
    }
    public function expressbillsender(){

        return $this->hasMany('addons\delivery\model\ExpressBillSender','delivery_id','id');

    }
    public function billsender(){

        return $this->hasOne('addons\delivery\model\ExpressBillSender','delivery_id','id');

    }
}
