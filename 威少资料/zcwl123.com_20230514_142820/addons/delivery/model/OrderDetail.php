<?php

namespace addons\delivery\model;

use think\Model;
use traits\model\SoftDelete;
/**
 * 标签模型
 */
class OrderDetail Extends Model
{
    use SoftDelete;
    protected $name = "delivery_order_detail";
    // 开启自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';
    // 定义时间戳字段名
    protected $createTime = 'addtime';
    protected $updateTime = '';
    protected $deleteTime = 'deletetime';
    // 追加属性
    protected $append = [

    ];
    protected static $config = [];

}
