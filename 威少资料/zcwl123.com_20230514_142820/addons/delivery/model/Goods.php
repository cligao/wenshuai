<?php

namespace addons\delivery\model;

use think\Model;
use traits\model\SoftDelete;
/**
 * 标签模型
 */
class Goods Extends Model
{
    use SoftDelete;
    protected $name = "delivery_goods";
    // 开启自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';
    // 定义时间戳字段名
    protected $createTime = 'addtime';
    protected $updateTime = '';
    protected $deleteTime = 'deletetime';
    // 追加属性
    protected $append = [
        'cat_name',
        'freight_name'
    ];
    protected static $config = [];
    public function getCatList()
    {
        return model('addons\delivery\model\Cat')->all();
    }
    public function getCatNameAttr($value, $data)
    {
        $model = model('addons\delivery\model\Cat');
        $info = $model->get($data['cat_id']);
        return $info['name']??'';
    }
    public function getFreightNameAttr($value, $data)
    {
        $model = model('addons\delivery\model\PostageRules');
        $info = $model->get($data['freight']);
        return $info['name']??'';
    }
    public function cat(){
        return $this->hasOne('addons\delivery\model\Cat','id','cat_id');
    }

}
