define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'timetable/course/index' + location.search,
                    add_url: 'timetable/course/add',
                    edit_url: 'timetable/course/edit',
                    del_url: 'timetable/course/del',
                    multi_url: 'timetable/course/multi',
                    import_url: 'timetable/course/import',
                    table: 'timetable_course',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'ciid',
                sortName: 'ciid',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'ciid', title: __('Ciid')},
                        {field: 'subject_id', title: __('Subject_id')},
                        {field: 'classroom', title: __('Classroom'), operate: 'LIKE'},
                        {field: 'days', title: __('Days')},
                        {field: 'nums', title: __('Nums')},
                        {field: 'duration', title: __('Duration')},
                        {field: 'attend', title: __('Attend'), operate: 'LIKE'},
                        {field: 'ctype', title: __('Ctype'), searchList: {"0":__('Ctype 0'),"1":__('Ctype 1'),"2":__('Ctype 2'),"3":__('Ctype 3'),"4":__('Ctype 4')}, formatter: Table.api.formatter.normal},
                        {field: 'tname', title: __('Tname'), operate: 'LIKE'},
                        {field: 'timetablesubjects.subject_id', title: __('Timetablesubjects.subject_id')},
                        {field: 'timetablesubjects.status', title: __('Timetablesubjects.status'), formatter: Table.api.formatter.status},
                        {field: 'timetablesubjects.sname', title: __('Timetablesubjects.sname'), operate: 'LIKE'},
                        {field: 'timetablesubjects.credit', title: __('Timetablesubjects.credit'), operate:'BETWEEN'},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});