package com.kp.common.log.enums;

/**
 * 操作状态
 * 
 * @author kp
 *
 */
public enum BusinessStatus
{
    /**
     * 成功
     */
    SUCCESS,

    /**
     * 失败
     */
    FAIL,
}
