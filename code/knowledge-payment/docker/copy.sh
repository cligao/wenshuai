#!/bin/sh

# 复制项目的文件到对应docker路径，便于一键生成镜像。
usage() {
	echo "Usage: sh copy.sh"
	exit 1
}


# copy sql
echo "begin copy sql "
cp ../sql/ry_20230223.sql ./mysql/db
cp ../sql/ry_config_20220929.sql ./mysql/db

# copy html
echo "begin copy html "
cp -r ../kp-ui/dist/** ./nginx/html/dist


# copy jar
echo "begin copy kp-gateway "
cp ../kp-gateway/target/kp-gateway.jar ./kp/gateway/jar

echo "begin copy kp-auth "
cp ../kp-auth/target/kp-auth.jar ./kp/auth/jar

echo "begin copy kp-visual "
cp ../kp-visual/kp-monitor/target/kp-visual-monitor.jar  ./kp/visual/monitor/jar

echo "begin copy kp-modules-system "
cp ../kp-modules/kp-system/target/kp-modules-system.jar ./kp/modules/system/jar

echo "begin copy kp-modules-file "
cp ../kp-modules/kp-file/target/kp-modules-file.jar ./kp/modules/file/jar

echo "begin copy kp-modules-job "
cp ../kp-modules/kp-job/target/kp-modules-job.jar ./kp/modules/job/jar

echo "begin copy kp-modules-gen "
cp ../kp-modules/kp-gen/target/kp-modules-gen.jar ./kp/modules/gen/jar

