package com.kp.video_stream_media.mapper;

import java.util.List;
import com.kp.video_stream_media.domain.Videos;

/**
 * 视频元数据Mapper接口
 * 
 * @author kp
 * @date 2023-05-12
 */
public interface VideosMapper 
{
    /**
     * 查询视频元数据
     * 
     * @param id 视频元数据主键
     * @return 视频元数据
     */
    public Videos selectVideosById(Long id);

    /**
     * 查询视频元数据列表
     * 
     * @param videos 视频元数据
     * @return 视频元数据集合
     */
    public List<Videos> selectVideosList(Videos videos);

    /**
     * 新增视频元数据
     * 
     * @param videos 视频元数据
     * @return 结果
     */
    public int insertVideos(Videos videos);

    /**
     * 修改视频元数据
     * 
     * @param videos 视频元数据
     * @return 结果
     */
    public int updateVideos(Videos videos);

    /**
     * 删除视频元数据
     * 
     * @param id 视频元数据主键
     * @return 结果
     */
    public int deleteVideosById(Long id);

    /**
     * 批量删除视频元数据
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteVideosByIds(Long[] ids);
}
