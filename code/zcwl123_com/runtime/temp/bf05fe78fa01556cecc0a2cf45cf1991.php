<?php if (!defined('THINK_PATH')) exit(); /*a:4:{s:75:"/www/wwwroot/zcwl123_com/public/../application/admin/view/vip/vip/edit.html";i:1681203074;s:67:"/www/wwwroot/zcwl123_com/application/admin/view/layout/default.html";i:1671020443;s:64:"/www/wwwroot/zcwl123_com/application/admin/view/common/meta.html";i:1671020443;s:66:"/www/wwwroot/zcwl123_com/application/admin/view/common/script.html";i:1671020443;}*/ ?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
<title><?php echo (isset($title) && ($title !== '')?$title:''); ?></title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
<meta name="renderer" content="webkit">
<meta name="referrer" content="never">
<meta name="robots" content="noindex, nofollow">

<link rel="shortcut icon" href="/assets/img/favicon.ico" />
<!-- Loading Bootstrap -->
<link href="/assets/css/backend<?php echo \think\Config::get('app_debug')?'':'.min'; ?>.css?v=<?php echo \think\Config::get('site.version'); ?>" rel="stylesheet">

<?php if(\think\Config::get('fastadmin.adminskin')): ?>
<link href="/assets/css/skins/<?php echo \think\Config::get('fastadmin.adminskin'); ?>.css?v=<?php echo \think\Config::get('site.version'); ?>" rel="stylesheet">
<?php endif; ?>

<!-- HTML5 shim, for IE6-8 support of HTML5 elements. All other JS at the end of file. -->
<!--[if lt IE 9]>
  <script src="/assets/js/html5shiv.js"></script>
  <script src="/assets/js/respond.min.js"></script>
<![endif]-->
<script type="text/javascript">
    var require = {
        config:  <?php echo json_encode($config); ?>
    };
</script>

    </head>

    <body class="inside-header inside-aside <?php echo defined('IS_DIALOG') && IS_DIALOG ? 'is-dialog' : ''; ?>">
        <div id="main" role="main">
            <div class="tab-content tab-addtabs">
                <div id="content">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <section class="content-header hide">
                                <h1>
                                    <?php echo __('Dashboard'); ?>
                                    <small><?php echo __('Control panel'); ?></small>
                                </h1>
                            </section>
                            <?php if(!IS_DIALOG && !\think\Config::get('fastadmin.multiplenav') && \think\Config::get('fastadmin.breadcrumb')): ?>
                            <!-- RIBBON -->
                            <div id="ribbon">
                                <ol class="breadcrumb pull-left">
                                    <?php if($auth->check('dashboard')): ?>
                                    <li><a href="dashboard" class="addtabsit"><i class="fa fa-dashboard"></i> <?php echo __('Dashboard'); ?></a></li>
                                    <?php endif; ?>
                                </ol>
                                <ol class="breadcrumb pull-right">
                                    <?php foreach($breadcrumb as $vo): ?>
                                    <li><a href="javascript:;" data-url="<?php echo $vo['url']; ?>"><?php echo $vo['title']; ?></a></li>
                                    <?php endforeach; ?>
                                </ol>
                            </div>
                            <!-- END RIBBON -->
                            <?php endif; ?>
                            <div class="content">
                                <style>
    .fieldlist .selectpicker {
        width: 100% !important;
        left: 0 !important;
    }
    .fieldlist.table tr td{
        text-align: center;
        padding: 8px 2px;
    }
    .fieldlist.table tr{
        padding: 2px 0;
    }
    .table .td-img{
        display:flex;
        align-items: center;
        justify-content: center;
    }
    .td-img .image-thumb{
        margin-right: 5px;
        width:32px;
        height:32px;
    }
    .td-img .image-thumb img{
        width:100%;
        height:100%;
    }
</style>
<form id="edit-form" class="form-horizontal" role="form" data-toggle="validator" method="POST" action="">

    <div class="form-group">
        <label class="control-label col-xs-12 col-sm-2"><?php echo __('Level'); ?>:</label>
        <div class="col-xs-12 col-sm-8">
            <input id="c-level" data-rule="required" class="form-control" name="row[level]" type="number" value="<?php echo htmlentities($row['level']); ?>">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-xs-12 col-sm-2"><?php echo __('Name'); ?>:</label>
        <div class="col-xs-12 col-sm-8">
            <input id="c-name" data-rule="required" class="form-control" name="row[name]" type="text" value="<?php echo htmlentities($row['name']); ?>">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-xs-12 col-sm-2"><?php echo __('Label'); ?>:</label>
        <div class="col-xs-12 col-sm-8">
            <input id="c-label" class="form-control" name="row[label]" type="text" value="<?php echo htmlentities($row['label']); ?>">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-xs-12 col-sm-2"><?php echo __('Intro'); ?>:</label>
        <div class="col-xs-12 col-sm-8">
            <input id="c-intro" class="form-control" name="row[intro]" type="text" value="<?php echo htmlentities($row['intro']); ?>">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-xs-12 col-sm-2"><?php echo __('Image'); ?>:</label>
        <div class="col-xs-12 col-sm-8">
            <div class="input-group">
                <input id="c-image" class="form-control" size="50" name="row[image]" type="text" value="<?php echo htmlentities($row['image']); ?>">
                <div class="input-group-addon no-border no-padding">
                    <span><button type="button" id="faupload-image" class="btn btn-danger faupload" data-input-id="c-image" data-mimetype="image/gif,image/jpeg,image/png,image/jpg,image/bmp,image/webp" data-multiple="false" data-preview-id="p-image"><i class="fa fa-upload"></i> <?php echo __('Upload'); ?></button></span>
                    <span><button type="button" id="fachoose-image" class="btn btn-primary fachoose" data-input-id="c-image" data-mimetype="image/*" data-multiple="false"><i class="fa fa-list"></i> <?php echo __('Choose'); ?></button></span>
                </div>
                <span class="msg-box n-right" for="c-image"></span>
            </div>
            <ul class="row list-inline faupload-preview" id="p-image"></ul>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-xs-12 col-sm-2"><?php echo __('Icon'); ?>:</label>
        <div class="col-xs-12 col-sm-8">
            <input id="c-icon" class="form-control" name="row[icon]" type="text" value="<?php echo htmlentities($row['icon']); ?>">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-xs-12 col-sm-2"><?php echo __('Content'); ?>:</label>
        <div class="col-xs-12 col-sm-8">
            <textarea id="c-content" class="form-control editor" rows="5" name="row[content]" cols="50"><?php echo htmlentities($row['content']); ?></textarea>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-xs-12 col-sm-2"><?php echo __('Pricedata'); ?>:</label>
        <div class="col-xs-12 col-sm-8">

            <table class="fieldlist table" id="pricetable" data-template="pricedata-tpl" data-tag="tr" data-name="row[pricedata]">
                <tr>
                    <td><?php echo __('标题'); ?></td>
                    <td><?php echo __('天数'); ?></td>
                    <td><?php echo __('价格'); ?></td>
                    <td><?php echo __('描述'); ?></td>
                    <td><?php echo __('单位'); ?></td>
                    <td width="50"><?php echo __('默认'); ?></td>
                    <td width="88"><?php echo __('操作'); ?></td>
                </tr>
                <tr>
                    <td colspan="6"></td>
                    <td>
                        <a href="javascript:;" class="btn btn-sm btn-success btn-append">
                            <i class="fa fa-plus"></i>
                            <?php echo __('Append'); ?>
                        </a>
                        <!--请注意 dd和textarea间不能存在其它任何元素，实际开发中textarea应该添加个hidden进行隐藏-->
                        <textarea name="row[pricedata]" class="form-control hide" cols="30" rows="5"><?php echo $row['pricedata']; ?></textarea>
                    </td>
                </tr>
            </table>
            <script id="pricedata-tpl" type="text/html">
                <tr>
                    <td><input type="text" name="<%=name%>[<%=index%>][title]" class="form-control" value="<%=row.title%>" placeholder="标题"/></td>
                    <td><input type="text" name="<%=name%>[<%=index%>][days]" class="form-control" value="<%=row.days%>" placeholder="天数" size="10"/></td>
                    <td><input type="number" name="<%=name%>[<%=index%>][price]" class="form-control" value="<%=row.price%>" placeholder="价格"/></td>
                    <td><input type="text" name="<%=name%>[<%=index%>][subtext]" class="form-control" value="<%=row.subtext%>" placeholder="描述"/></td>
                    <td><input type="text" name="<%=name%>[<%=index%>][unit]" class="form-control" value="<%=row.unit%>" placeholder="单位"/></td>
                    <td>
                        <select class="form-control selectpicker" name="<%=name%>[<%=index%>][default]">
                            <option value="0" <%=row.default==0?'selected':''%> >否</option>
                            <option value="1" <%=row.default==1?'selected':''%> >是</option>
                        </select>
                    </td>
                   <td>
                        <!--下面的两个按钮务必保留-->
                        <span class="btn btn-sm btn-danger btn-remove"><i class="fa fa-times"></i></span>
                        <span class="btn btn-sm btn-primary btn-dragsort"><i class="fa fa-arrows"></i></span>
                   </td>
                </tr>
            </script>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-xs-12 col-sm-2"><?php echo __('Rightdata'); ?>:</label>
        <div class="col-xs-12 col-sm-8">
            <table class="fieldlist table" id="righttable" data-template="rightdata-tpl" data-tag="tr" data-name="row[rightdata]">
                <tr>
                    <td><?php echo __('名称'); ?></td>
                    <td width="50%"><?php echo __('介绍'); ?></td>
                    <td><?php echo __('图标'); ?></td>
                    <td width="88"><?php echo __('操作'); ?></td>
                </tr>
                <tr>
                    <td colspan="3"></td>
                    <td>
                        <a href="javascript:;" class="btn btn-sm btn-success btn-append">
                            <i class="fa fa-plus"></i>
                            <?php echo __('Append'); ?>
                        </a>
                        <!--请注意 dd和textarea间不能存在其它任何元素，实际开发中textarea应该添加个hidden进行隐藏-->
                        <textarea name="row[rightdata]" class="form-control hide" cols="30" rows="5"><?php echo $row['rightdata']; ?></textarea>
                    </td>
                </tr>
            </table>
            <script id="rightdata-tpl" type="text/html">
                <tr>
                    <td><input type="text" name="<%=name%>[<%=index%>][text]" class="form-control" value="<%=row.text%>" placeholder="名称"/></td>
                    <td><input type="text" name="<%=name%>[<%=index%>][intro]" class="form-control" value="<%=row.intro%>" placeholder="介绍" size="10"/></td>
                    <td class="td-img">
                        <div class="image-thumb" id="p-image-<%=index%>" data-template="p-image-tpl">
                            <img src="<%=row.image ? row.image : '/assets/img/blank.gif'%>" />
                        </div>
                        <div class="input-group">
                            <input type="hidden" id="c-image-<%=index%>" name="<%=name%>[<%=index%>][image]" class="form-control" value="<%=row.image%>"/>
                            <div class="input-group-addon no-border no-padding">
                                <span>
                                    <button type="button" id="faupload-image-<%=index%>" class="btn btn-danger faupload"
                                    data-input-id="c-image-<%=index%>" data-mimetype="image/gif,image/jpeg,image/png,image/jpg,image/bmp,image/webp"
                                        data-multiple="false" data-preview-id="p-image-<%=index%>"><i class="fa fa-upload"></i>
                                        <?php echo __('Upload'); ?></button></span>
                                <span><button type="button" id="fachoose-image-<%=index%>" class="btn btn-primary fachoose"
                                        data-input-id="c-image-<%=index%>" data-mimetype="image/*" data-multiple="false"><i
                                            class="fa fa-list"></i> <?php echo __('Choose'); ?></button></span>
                            </div>
                            <span class="msg-box n-right" for="c-image-<%=index%>"></span>
                         </div>
                    </td>
                   <td>
                        <!--下面的两个按钮务必保留-->
                        <span class="btn btn-sm btn-danger btn-remove"><i class="fa fa-times"></i></span>
                        <span class="btn btn-sm btn-primary btn-dragsort"><i class="fa fa-arrows"></i></span>
                   </td>
                </tr>
            </script>
            <script id="p-image-tpl" type="text/html">
                <a href="<%=fullurl%>" data-url="<%=url%>" target="_blank" class="">
                    <img src="<%=fullurl%>"/>
                </a>
            </script>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-xs-12 col-sm-2"><?php echo __('Sales'); ?>:</label>
        <div class="col-xs-12 col-sm-8">
            <input id="c-sales" class="form-control" name="row[sales]" type="number" value="<?php echo htmlentities($row['sales']); ?>">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-xs-12 col-sm-2"><?php echo __('Status'); ?>:</label>
        <div class="col-xs-12 col-sm-8">

            <div class="radio">
            <?php if(is_array($statusList) || $statusList instanceof \think\Collection || $statusList instanceof \think\Paginator): if( count($statusList)==0 ) : echo "" ;else: foreach($statusList as $key=>$vo): ?>
            <label for="row[status]-<?php echo $key; ?>"><input id="row[status]-<?php echo $key; ?>" name="row[status]" type="radio" value="<?php echo $key; ?>" <?php if(in_array(($key), is_array($row['status'])?$row['status']:explode(',',$row['status']))): ?>checked<?php endif; ?> /> <?php echo $vo; ?></label>
            <?php endforeach; endif; else: echo "" ;endif; ?>
            </div>

        </div>
    </div>
    <div class="form-group layer-footer">
        <label class="control-label col-xs-12 col-sm-2"></label>
        <div class="col-xs-12 col-sm-8">
            <button type="submit" class="btn btn-primary btn-embossed disabled"><?php echo __('OK'); ?></button>
            <button type="reset" class="btn btn-default btn-embossed"><?php echo __('Reset'); ?></button>
        </div>
    </div>
</form>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script src="/assets/js/require<?php echo \think\Config::get('app_debug')?'':'.min'; ?>.js" data-main="/assets/js/require-backend<?php echo \think\Config::get('app_debug')?'':'.min'; ?>.js?v=<?php echo htmlentities($site['version']); ?>"></script>
    </body>
</html>
