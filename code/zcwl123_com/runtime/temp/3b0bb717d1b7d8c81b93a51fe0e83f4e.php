<?php if (!defined('THINK_PATH')) exit(); /*a:11:{s:63:"/www/wwwroot/zcwl123_com/addons/cms/view/default/show_news.html";i:1650535601;s:67:"/www/wwwroot/zcwl123_com/addons/cms/view/default/common/layout.html";i:1668416891;s:68:"/www/wwwroot/zcwl123_com/addons/cms/view/default/common/payment.html";i:1631951623;s:68:"/www/wwwroot/zcwl123_com/addons/cms/view/default/common/paytype.html";i:1631795612;s:67:"/www/wwwroot/zcwl123_com/addons/cms/view/default/common/donate.html";i:1635320658;s:66:"/www/wwwroot/zcwl123_com/addons/cms/view/default/common/share.html";i:1631091567;s:69:"/www/wwwroot/zcwl123_com/addons/cms/view/default/common/metainfo.html";i:1650535601;s:68:"/www/wwwroot/zcwl123_com/addons/cms/view/default/common/related.html";i:1650535601;s:68:"/www/wwwroot/zcwl123_com/addons/cms/view/default/common/comment.html";i:1650535601;s:71:"/www/wwwroot/zcwl123_com/addons/cms/view/default/common/authorinfo.html";i:1650535601;s:68:"/www/wwwroot/zcwl123_com/addons/cms/view/default/common/sidebar.html";i:1650535601;}*/ ?>
<!DOCTYPE html>
<!--[if lt IE 7]>
<html class="lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>
<html class="lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>
<html class="lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class=""> <!--<![endif]-->
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,Chrome=1">
    <meta name="viewport" content="width=device-width,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no">
    <meta name="renderer" content="webkit">
    <title><?php echo htmlentities(\think\Config::get('cms.title')); ?> - <?php echo \think\Config::get('cms.sitename'); ?></title>
    <meta name="keywords" content="<?php echo htmlentities(\think\Config::get('cms.keywords')); ?>"/>
    <meta name="description" content="<?php echo htmlentities(\think\Config::get('cms.description')); ?>"/>

    <link rel="shortcut icon" href="/assets/img/favicon.ico" type="image/x-icon"/>
    <link rel="stylesheet" media="screen" href="/assets/css/bootstrap.min.css?v=<?php echo $site['version']; ?>"/>
    <link rel="stylesheet" media="screen" href="/assets/libs/font-awesome/css/font-awesome.min.css?v=<?php echo $site['version']; ?>"/>
    <link rel="stylesheet" media="screen" href="/assets/libs/fastadmin-layer/dist/theme/default/layer.css?v=<?php echo $site['version']; ?>"/>
    <link rel="stylesheet" media="screen" href="/assets/addons/cms/css/swiper.min.css?v=<?php echo $site['version']; ?>">
    <link rel="stylesheet" media="screen" href="/assets/addons/cms/css/share.min.css?v=<?php echo $site['version']; ?>">
    <link rel="stylesheet" media="screen" href="/assets/addons/cms/css/iconfont.css?v=<?php echo $site['version']; ?>">
    <link rel="stylesheet" media="screen" href="/assets/addons/cms/css/common.css?v=<?php echo $site['version']; ?>"/>

    <!--分享-->
    <meta property="og:title" content="<?php echo htmlentities(\think\Config::get('cms.title')); ?>"/>
    <meta property="og:image" content="<?php echo htmlentities(\think\Config::get('cms.image')); ?>"/>
    <meta property="og:description" content="<?php echo htmlentities(\think\Config::get('cms.description')); ?>"/>

    {__STYLE__}

    <!--[if lt IE 9]>
    <script src="/libs/html5shiv.js"></script>
    <script src="/libs/respond.min.js"></script>
    <![endif]-->

    
</head>
<body class="group-page skin-white">

<header class="header">
    <!-- S 导航 -->
    <nav class="navbar navbar-default navbar-white navbar-fixed-top" role="navigation">
        <div class="container">

            <div class="navbar-header">
                <button type="button" class="navbar-toggle sidebar-toggle">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?php echo \think\Config::get('cms.indexurl'); ?>"><img src="<?php echo cdnurl((\think\Config::get('cms.sitelogo') ?: '/assets/addons/cms/img/logo.png')); ?>" style="height:100%;" alt=""></a>
            </div>

            <div class="collapse navbar-collapse" id="navbar-collapse">
                <ul class="nav navbar-nav" data-current="<?php echo (isset($__CHANNEL__['id']) && ($__CHANNEL__['id'] !== '')?$__CHANNEL__['id']:0); ?>">
                    <!--如果你需要自定义NAV,可使用channellist标签来完成,这里只设置了2级,如果显示无限级,请使用cms:nav标签-->
                    <?php $__4TAvL95uDX__ = \addons\cms\model\Channel::getChannelList(["id"=>"nav","type"=>"top","condition"=>"1=isnav"]); if(is_array($__4TAvL95uDX__) || $__4TAvL95uDX__ instanceof \think\Collection || $__4TAvL95uDX__ instanceof \think\Paginator): $i = 0; $__LIST__ = $__4TAvL95uDX__;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$nav): $mod = ($i % 2 );++$i;?>
                    <!--判断是否有子级或高亮当前栏目-->
                    <li class="<?php if($nav['has_child']): ?>dropdown<?php endif; if($nav->is_active): ?> active<?php endif; ?>">
                        <a href="<?php echo $nav['url']; ?>" <?php if($nav['has_child']): ?> data-toggle="dropdown" <?php endif; ?>><?php echo htmlentities($nav['name']); if($nav['has_nav_child']): ?> <b class="caret"></b><?php endif; ?></a>
                        <ul class="dropdown-menu <?php if(!$nav['has_nav_child']): ?>hidden<?php endif; ?>" role="menu">
                            <?php $__OvnUSAHtYM__ = \addons\cms\model\Channel::getChannelList(["id"=>"sub","type"=>"son","typeid"=>$nav['id'],"condition"=>"1=isnav"]); if(is_array($__OvnUSAHtYM__) || $__OvnUSAHtYM__ instanceof \think\Collection || $__OvnUSAHtYM__ instanceof \think\Paginator): $i = 0; $__LIST__ = $__OvnUSAHtYM__;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$sub): $mod = ($i % 2 );++$i;?>
                            <li><a href="<?php echo $sub['url']; ?>"><?php echo htmlentities($sub['name']); ?></a></li>
                            <?php endforeach; endif; else: echo "" ;endif; $__LASTLIST__=$__OvnUSAHtYM__; ?>
                        </ul>
                    </li>
                    <?php endforeach; endif; else: echo "" ;endif; $__LASTLIST__=$__4TAvL95uDX__; ?>

                    <!--如果需要无限级请使用cms:nav标签-->
                    
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <form class="form-inline navbar-form" action="<?php echo addon_url('cms/search/index'); ?>" method="get">
                            <div class="form-search hidden-sm">
                                <input class="form-control" name="q" data-suggestion-url="<?php echo addon_url('cms/search/suggestion'); ?>" type="search" id="searchinput" value="<?php echo htmlentities((\think\Request::instance()->request('q') ?: '')); ?>" placeholder="搜索">
                                <div class="search-icon"></div>
                            </div>
                            <?php echo token('__searchtoken__'); ?>
                        </form>
                    </li>
                    <?php if(config('fastadmin.usercenter')): ?>
                    <li class="dropdown navbar-userinfo">
                        <?php if($user): ?>
                        <a href="<?php echo url('index/user/index'); ?>" class="dropdown-toggle" data-toggle="dropdown">
                            <span class="avatar-img pull-left"><img src="<?php echo htmlentities(cdnurl($user['avatar'])); ?>" style="width:27px;height:27px;border-radius:50%;" alt=""></span>
                            <span class="visible-xs pull-left ml-2 pt-1"><?php echo htmlentities($user['nickname']); ?> <b class="caret"></b></span>
                        </a>
                        <?php else: ?>
                        <a href="<?php echo url('index/user/index'); ?>" class="dropdown-toggle" data-toggle="dropdown">会员<span class="hidden-sm">中心</span> <b class="caret"></b></a>
                        <?php endif; ?>
                        <ul class="dropdown-menu">
                            <?php if($user): ?>
                            <li><a href="<?php echo url('index/user/index'); ?>"><i class="fa fa-user fa-fw"></i> 会员中心</a></li>
                            <li><a href="<?php echo addon_url('cms/user/index', [':id'=>$user['id']]); ?>"><i class="fa fa-user-circle fa-fw"></i> 我的个人主页</a></li>
                            <?php $sidenav = array_filter(explode(',', config('cms.usersidenav'))); if(in_array('myarchives', $sidenav)): ?>
                            <li><a href="<?php echo url('index/cms.archives/my'); ?>"><i class="fa fa-list fa-fw"></i> 我发布的文档</a></li>
                            <?php endif; if(in_array('postarchives', $sidenav)): ?>
                            <li><a href="<?php echo url('index/cms.archives/post'); ?>"><i class="fa fa-pencil fa-fw"></i> 发布文档</a></li>
                            <?php endif; if(in_array('myorder', $sidenav)): ?>
                            <li><a href="<?php echo url('index/cms.order/index'); ?>"><i class="fa fa-shopping-bag fa-fw"></i> 我的消费订单</a></li>
                            <?php endif; if(in_array('mycomment', $sidenav)): ?>
                            <li><a href="<?php echo url('index/cms.comment/index'); ?>"><i class="fa fa-comments fa-fw"></i> 我发表的评论</a></li>
                            <?php endif; if(in_array('mycollection', $sidenav)): ?>
                            <li><a href="<?php echo url('index/cms.collection/index'); ?>"><i class="fa fa-bookmark fa-fw"></i> 我的收藏</a></li>
                            <?php endif; ?>
                            <li><a href="<?php echo url('index/user/logout'); ?>"><i class="fa fa-sign-out fa-fw"></i> 退出</a></li>
                            <?php else: ?>
                            <li><a href="<?php echo url('index/user/login'); ?>"><i class="fa fa-sign-in fa-fw"></i> 登录</a></li>
                            <li><a href="<?php echo url('index/user/register'); ?>"><i class="fa fa-user-o fa-fw"></i> 注册</a></li>
                            <?php endif; ?>
                        </ul>
                    </li>
                    <?php endif; ?>
                </ul>
            </div>

        </div>
    </nav>
    <!-- E 导航 -->

</header>

<main class="main-content">
    

<div class="container" id="content-container">

    <div class="row">

        <main class="col-xs-12 col-md-8">
            <div class="panel panel-default article-content">
                <div class="panel-heading">
                    <ol class="breadcrumb">
                        <!-- S 面包屑导航 -->
                        <?php $__LNDdZYepQa__ = \addons\cms\model\Channel::getBreadcrumb($__CHANNEL__??[], $__ARCHIVES__??[], $__TAGS__??[], $__PAGE__??[], $__DIYFORM__??[], $__SPECIAL__??[]); if(is_array($__LNDdZYepQa__) || $__LNDdZYepQa__ instanceof \think\Collection || $__LNDdZYepQa__ instanceof \think\Paginator): $i = 0; $__LIST__ = $__LNDdZYepQa__;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$item): $mod = ($i % 2 );++$i;?>
                        <li><a href="<?php echo $item['url']; ?>"><?php echo htmlentities($item['name']); ?></a></li>
                        <?php endforeach; endif; else: echo "" ;endif; $__LASTLIST__=$__LNDdZYepQa__; ?>
                        <!-- E 面包屑导航 -->
                    </ol>
                </div>
                <div class="panel-body">
                    <div class="article-metas">
                        <h1 class="metas-title" <?php if($__ARCHIVES__['style']): ?>style="<?php echo $__ARCHIVES__['style_text']; ?>" <?php endif; ?>><?php echo htmlentities($__ARCHIVES__['title']); ?></h1>

                        <div class="metas-body">
                            <?php if(isset($__ARCHIVES__['author']) && $__ARCHIVES__['author']): ?>
                            <span>
                                <i class="fa fa-user"></i> <?php echo htmlentities($__ARCHIVES__['author']); ?>
                            </span>
                            <?php endif; ?>
                            <span class="views-num">
                                <i class="fa fa-eye"></i> <?php echo $__ARCHIVES__['views']; ?> 阅读
                            </span>
                            <span class="comment-num">
                                <i class="fa fa-comments"></i> <?php echo $__ARCHIVES__['comments']; ?> 评论
                            </span>
                            <span class="like-num">
                                <i class="fa fa-thumbs-o-up"></i>
                                <span class="js-like-num"> <?php echo $__ARCHIVES__['likes']; ?> 点赞
                                </span>
                            </span>
                        </div>

                    </div>

                    <div class="article-text">
                        <!-- S 正文 -->
                        <p>
                            <?php if($__ARCHIVES__['is_paid_part_of_content'] || $__ARCHIVES__['ispaid']): ?>
                            <?php echo $__ARCHIVES__['content']; endif; ?>
                        </p>
                        <!-- E 正文 -->
                    </div>

                    <!-- S 付费阅读 -->
<?php if(!$__ARCHIVES__['ispaid']): ?>
<div class="article-pay">
    <?php if($__CHANNEL__['vip']>0 && (!$user || $user['vip']<$__CHANNEL__['vip'])): ?>
    <div class="alert alert-danger">
        <strong>温馨提示!</strong> 升级 <b>VIP <?php echo $__CHANNEL__['vip']; ?></b> 免费浏览，你当前
        <?php if($user): ?>
            <b>VIP <?php echo (isset($user['vip']) && ($user['vip'] !== '')?$user['vip']:0); ?></b>
        <?php else: ?>
            未登录
        <?php endif; ?>
        <div class="mt-3">
            <a href="<?php echo url('index/vip/viplist'); ?>" target="_blank" class="btn btn-warning">
                <i class="fa fa-diamond"></i> 升级VIP <?php echo $__CHANNEL__['vip']; ?>
            </a>
        </div>
    </div>
    <?php endif; if($__ARCHIVES__['price']>0): ?>
    <div class="alert alert-danger">
        <strong>温馨提示!</strong> 你需要支付 <b>￥<?php echo $__ARCHIVES__['price']; ?></b> 元后才能查看付费内容
        <div class="mt-3">
            <!--@formatter:off-->
<?php if(stripos(config('cms.paytypelist'),'wechat')!==false): ?>
    <a href="<?php echo addon_url('cms/order/submit', ['id'=>$__ARCHIVES__['id'],'paytype'=>'wechat']); ?>" <?php if(!$isWechat): ?>target="_blank"<?php endif; ?> class="btn btn-success btn-paynow"><i class="fa fa-wechat"></i> 微信支付</a>
<?php endif; if(stripos(config('cms.paytypelist'),'alipay')!==false): ?>
    <a href="<?php echo addon_url('cms/order/submit', ['id'=>$__ARCHIVES__['id'],'paytype'=>'alipay']); ?>" <?php if(!$isWechat): ?>target="_blank"<?php endif; ?> class="btn btn-primary btn-paynow"><i class="fa fa-money"></i> 支付宝支付</a>
<?php endif; if(stripos(config('cms.paytypelist'),'balance')!==false): ?>
    <a href="<?php echo addon_url('cms/order/submit', ['id'=>$__ARCHIVES__['id'],'paytype'=>'balance']); ?>" class="btn btn-warning btn-balance" data-price="<?php echo $__ARCHIVES__['price']; ?>"><i class="fa fa-dollar"></i> 余额支付</a>
<?php endif; ?>
<!--@formatter:on-->

        </div>
    </div>
    <?php endif; ?>
</div>
<?php endif; ?>
<!-- E 付费阅读 -->


                    <!-- S 点赞 -->
<div class="article-donate">
    <a href="javascript:" class="btn btn-primary btn-like btn-lg" data-action="vote" data-type="like" data-id="<?php echo $__ARCHIVES__['id']; ?>" data-tag="archives"><i class="fa fa-thumbs-up"></i> 点赞(<span><?php echo $__ARCHIVES__['likes']; ?></span>)</a>
    <?php if(config('cms.donateimage')): ?>
    <a href="javascript:" class="btn btn-outline-primary btn-donate btn-lg" data-action="donate" data-id="<?php echo $__ARCHIVES__['id']; ?>" data-image="<?php echo cdnurl($config['donateimage']); ?>"><i class="fa fa-cny"></i> 打赏</a>
    <?php endif; ?>
</div>
<!-- E 点赞 -->


                    
<?php $image = $__ARCHIVES__['image']; $aid = $__ARCHIVES__['id']; ?>

<!-- S 分享 -->
<div class="social-share text-center mt-2 mb-1" data-initialized="true" data-mode="prepend" data-image="<?php echo $image; ?>">
    <a href="javascript:" class="social-share-icon icon-heart addbookbark" data-type="archives" data-aid="<?php echo $aid; ?>" data-action="<?php echo addon_url('cms/ajax/collection'); ?>"></a>
    <a href="#" class="social-share-icon icon-weibo" target="_blank"></a>
    <a href="#" class="social-share-icon icon-qq" target="_blank"></a>
    <a href="#" class="social-share-icon icon-qzone" target="_blank"></a>
    <a href="javascript:" class="social-share-icon icon-wechat"></a>
</div>
<!-- E 分享 -->


                    <div class="entry-meta">
    <ul>
        <!-- S 归档 -->
        <li><?php echo __('Article category'); ?>：<a href="<?php echo $__CHANNEL__['url']; ?>"><?php echo htmlentities($__CHANNEL__['name']); ?></a></li>
        <li><?php echo __('Article tags'); ?>：<?php if(is_array($__ARCHIVES__['taglist']) || $__ARCHIVES__['taglist'] instanceof \think\Collection || $__ARCHIVES__['taglist'] instanceof \think\Paginator): $i = 0; $__LIST__ = $__ARCHIVES__['taglist'];if( count($__LIST__)==0 ) : echo "无" ;else: foreach($__LIST__ as $key=>$tag): $mod = ($i % 2 );++$i;?><a href="<?php echo $tag['url']; ?>" class="tag" rel="tag"><?php echo htmlentities($tag['name']); ?></a><?php endforeach; endif; else: echo "无" ;endif; ?></li>
        <li><?php echo __('Article views'); ?>：<span><?php echo $__ARCHIVES__['views']; ?></span> 次浏览</li>
        <li><?php echo __('Post date'); ?>：<?php echo datetime($__ARCHIVES__['publishtime']); ?></li>
        <li><?php echo __('Article url'); ?>：<a href="<?php echo $__ARCHIVES__['fullurl']; ?>"><?php echo $__ARCHIVES__['fullurl']; ?></a></li>
        <!-- S 归档 -->
    </ul>

    <ul class="article-prevnext">
        <!-- S 上一篇下一篇 -->
        <?php $prev = \addons\cms\model\Archives::getPrevNext(["id"=>"prev","type"=>"prev","archives"=>$__ARCHIVES__['id'],"channel"=>$__CHANNEL__['id']]);if($prev): ?>
        <li>
            <span><?php echo __('Prev'); ?> &gt;</span>
            <a href="<?php echo $prev['url']; ?>"><?php echo htmlentities($prev['title']); ?></a>
        </li>
        <?php else:endif;$next = \addons\cms\model\Archives::getPrevNext(["id"=>"next","type"=>"next","archives"=>$__ARCHIVES__['id'],"channel"=>$__CHANNEL__['id']]);if($next): ?>
        <li>
            <span><?php echo __('Next'); ?> &gt;</span>
            <a href="<?php echo $next['url']; ?>"><?php echo htmlentities($next['title']); ?></a>
        </li>
        <?php else:endif;?>
        <!-- E 上一篇下一篇 -->
    </ul>
</div>


                    <div class="related-article">
    <div class="row">
        <!-- S 相关文章 -->
        <?php $__akmr7VhGC3__ = \addons\cms\model\Archives::getArchivesList(["id"=>"relate","tags"=>$__ARCHIVES__['tags'],"model"=>$__ARCHIVES__['model_id'],"row"=>"4"]); if(is_array($__akmr7VhGC3__) || $__akmr7VhGC3__ instanceof \think\Collection || $__akmr7VhGC3__ instanceof \think\Paginator): $i = 0; $__LIST__ = $__akmr7VhGC3__;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$relate): $mod = ($i % 2 );++$i;?>
        <div class="col-sm-3 col-xs-6">
            <a href="<?php echo $relate['url']; ?>" class="img-zoom">
                <div class="embed-responsive embed-responsive-4by3">
                    <img src="<?php echo $relate['image']; ?>" alt="<?php echo htmlentities($relate['title']); ?>" class="embed-responsive-item">
                </div>
            </a>
            <h5 class="text-center"><a href="<?php echo $relate['url']; ?>"><?php echo htmlentities($relate['title']); ?></a></h5>
        </div>
        <?php endforeach; endif; else: echo "" ;endif; $__LASTLIST__=$__akmr7VhGC3__; ?>
        <!-- E 相关文章 -->
    </div>
</div>


                    <div class="clearfix"></div>
                </div>
            </div>

            <?php if($config['iscomment'] && config('fastadmin.usercenter')): ?>
            <div class="panel panel-default" id="comments">
                <div class="panel-heading">
                    <h3 class="panel-title"><?php echo __('Comment list'); ?>
                        <small>共有 <span><?php echo $__ARCHIVES__['comments']; ?></span> 条评论</small>
                    </h3>
                </div>
                <div class="panel-body">
                    <?php if($__ARCHIVES__['iscomment']): ?>
                    <div id="comment-container">
    <!-- S 评论列表 -->
    <div id="commentlist">
        <?php $aid = $__ARCHIVES__['eid']; $__COMMENTLIST__ = \addons\cms\model\Comment::getCommentList(["id"=>"comment","type"=>"archives","aid"=>"$aid","pagesize"=>"10"]); if(is_array($__COMMENTLIST__) || $__COMMENTLIST__ instanceof \think\Collection || $__COMMENTLIST__ instanceof \think\Paginator): $i = 0; $__LIST__ = $__COMMENTLIST__;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$comment): $mod = ($i % 2 );++$i;?>
        <dl id="comment-<?php echo $comment['id']; ?>">
            <dt><a href="<?php echo $comment['user']['url']; ?>" target="_blank"><img src='<?php echo htmlentities(cdnurl($comment['user']['avatar'])); ?>'/></a></dt>
            <dd>
                <div class="parent">
                    <cite><a href='<?php echo $comment['user']['url']; ?>' target="_blank"><?php echo htmlentities($comment['user']['nickname']); ?></a></cite>
                    <small> <?php echo human_date($comment['createtime']); ?> <a href="javascript:;" data-id="<?php echo $comment['id']; ?>" title="@<?php echo htmlentities($comment['user']['nickname']); ?> " class="reply">回复TA</a></small>
                    <p><?php echo htmlentities($comment['content']); ?></p>
                </div>
            </dd>
            <div class="clearfix"></div>
        </dl>
        <?php endforeach; endif; else: echo "" ;endif; $__LASTLIST__=$__COMMENTLIST__; if($__COMMENTLIST__->isEmpty()): ?>
        <div class="loadmore loadmore-line loadmore-nodata"><span class="loadmore-tips">暂无评论</span></div>
        <?php endif; ?>
    </div>
    <!-- E 评论列表 -->

    <!-- S 评论分页 -->
    <div id="commentpager" class="text-center">
        <?php echo $__COMMENTLIST__->render(["type"=>"full"]); ?>
    </div>
    <!-- E 评论分页 -->

    <!-- S 发表评论 -->
    <div id="postcomment">
        <h3>发表评论 <a href="javascript:;">
            <small>取消回复</small>
        </a></h3>
        <form action="<?php echo addon_url('cms/comment/post'); ?>" method="post" id="postform">
            <?php echo token(); ?>
            <input type="hidden" name="type" value="archives"/>
            <input type="hidden" name="aid" value="<?php echo $__ARCHIVES__['eid']; ?>"/>
            <input type="hidden" name="pid" id="pid" value="0"/>
            <div class="form-group">
                <textarea name="content" class="form-control" <?php if(!$user): ?>disabled placeholder="请登录后再发表评论" <?php endif; ?> id="commentcontent" cols="6" rows="5" tabindex="4"></textarea>
            </div>
            <?php if(!$user): ?>
            <div class="form-group">
                <a href="<?php echo url('index/user/login'); ?>" class="btn btn-primary">登录</a>
                <a href="<?php echo url('index/user/register'); ?>" class="btn btn-outline-primary">注册新账号</a>
            </div>
            <?php else: ?>
            <div class="form-group">
                <input name="submit" type="submit" id="submit" tabindex="5" value="提交评论(Ctrl+回车)" class="btn btn-primary"/>
                <span id="actiontips"></span>
            </div>
            <div class="checkbox">
                <label>
                    <input name="subscribe" type="checkbox" class="checkbox" tabindex="7" checked value="1"/> 有人回复时邮件通知我
                </label>
            </div>
            <?php endif; ?>
        </form>
    </div>
    <!-- E 发表评论 -->
</div>

                    <?php else: ?>
                    <div class="text-muted text-center">评论功能已关闭</div>
                    <?php endif; ?>
                </div>
            </div>
            <?php endif; ?>

        </main>

        <aside class="col-xs-12 col-md-4">
            <!--@formatter:off-->
<?php if($config['userpage'] && $__ARCHIVES__['user']): ?>
<!-- S 关于作者 -->
<div class="panel panel-default about-author no-padding" data-id="<?php echo $__ARCHIVES__['user']['id']; ?>" itemProp="author" itemscope="" itemType="http://schema.org/Person">
    <meta itemProp="name" content="<?php echo htmlentities($__ARCHIVES__['user']['nickname']); ?>"/>
    <meta itemProp="image" content="<?php echo htmlentities(cdnurl($__ARCHIVES__['user']['avatar'])); ?>"/>
    <meta itemProp="url" content="<?php echo $__ARCHIVES__['user']['url']; ?>"/>
    <div class="panel-body no-padding">
        <div class="author-card">
            <div class="author-head" <?php if(\think\Config::get('cms.default_author_head_img')): ?>style="background-image:url('<?php echo cdnurl(\think\Config::get('cms.default_author_head_img')); ?>');"<?php endif; ?>></div>

            <div class="author-avatar">
                <a href="<?php echo $__ARCHIVES__['user']['url']; ?>">
                    <img src="<?php echo htmlentities(cdnurl($__ARCHIVES__['user']['avatar'])); ?>">
                </a>
            </div>

            <div class="author-basic">
                <div class="author-nickname">
                    <a href="<?php echo $__ARCHIVES__['user']['url']; ?>"><?php echo htmlentities($__ARCHIVES__['user']['nickname']); ?></a>
                </div>
                <span class="text-muted"><?php echo htmlentities((isset($__ARCHIVES__['user']['bio']) && ($__ARCHIVES__['user']['bio'] !== '')?$__ARCHIVES__['user']['bio']:"这家伙很懒，什么也没写！")); ?></span>
            </div>

            <div class="row author-statistics">
                <div class="col-xs-4">
                    <div class="statistics-text">文章</div>
                    <div class="statistics-nums"><a href="<?php echo addon_url('cms/user/index',[':id'=>$__ARCHIVES__['user_id']],false); ?>/archives"><?php echo (isset($__ARCHIVES__['user']['archives']) && ($__ARCHIVES__['user']['archives'] !== '')?$__ARCHIVES__['user']['archives']:0); ?></a></div>
                </div>
                <div class="col-xs-4">
                    <div class="statistics-text">评论</div>
                    <div class="statistics-nums"><a href="<?php echo addon_url('cms/user/index',[':id'=>$__ARCHIVES__['user_id']],false); ?>/comment"><?php echo (isset($__ARCHIVES__['user']['comments']) && ($__ARCHIVES__['user']['comments'] !== '')?$__ARCHIVES__['user']['comments']:0); ?></a></div>
                </div>
                <div class="col-xs-4">
                    <div class="statistics-text">加入时间</div>
                    <div class="statistics-nums"><?php echo human_date((isset($__ARCHIVES__['user']['jointime']) && ($__ARCHIVES__['user']['jointime'] !== '')?$__ARCHIVES__['user']['jointime']:0)); ?></div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- E 关于作者 -->
<?php endif; ?>
<!--@formatter:on-->

            
<div class="panel panel-blockimg">
    <?php if((!0 || time()>0)&&(!0 || time()<0)): ?><?php echo cache('taglib_cms_block_content_8'); endif; ?>
</div>

<!-- S 热门资讯 -->
<div class="panel panel-default hot-article">
    <div class="panel-heading">
        <h3 class="panel-title"><?php echo __('Recommend news'); ?></h3>
    </div>
    <div class="panel-body">
        <?php $__DxcL4YvnC3__ = \addons\cms\model\Archives::getArchivesList(["id"=>"item","model"=>"1","row"=>"10","flag"=>"recommend","orderby"=>"id","orderway"=>"asc"]); if(is_array($__DxcL4YvnC3__) || $__DxcL4YvnC3__ instanceof \think\Collection || $__DxcL4YvnC3__ instanceof \think\Paginator): $i = 0; $__LIST__ = $__DxcL4YvnC3__;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$item): $mod = ($i % 2 );++$i;?>
        <div class="media media-number">
            <div class="media-left">
                <span class="num tag"><?php echo $i; ?></span>
            </div>
            <div class="media-body">
                <a class="link-dark" href="<?php echo $item['url']; ?>" title="<?php echo htmlentities($item['title']); ?>"><?php echo htmlentities($item['title']); ?></a>
            </div>
        </div>
        <?php endforeach; endif; else: echo "" ;endif; $__LASTLIST__=$__DxcL4YvnC3__; ?>
    </div>
</div>
<!-- E 热门资讯 -->

<div class="panel panel-blockimg">
    <?php if((!0 || time()>0)&&(!0 || time()<0)): ?><?php echo cache('taglib_cms_block_content_9'); endif; ?>
</div>

<!-- S 热门标签 -->
<div class="panel panel-default hot-tags">
    <div class="panel-heading">
        <h3 class="panel-title"><?php echo __('Hot tags'); ?></h3>
    </div>
    <div class="panel-body">
        <div class="tags">
            <?php $__6N5iGqaXz2__ = \addons\cms\model\Tag::getTagList(["id"=>"tag","orderby"=>"rand","limit"=>"30"]); if(is_array($__6N5iGqaXz2__) || $__6N5iGqaXz2__ instanceof \think\Collection || $__6N5iGqaXz2__ instanceof \think\Paginator): $i = 0; $__LIST__ = $__6N5iGqaXz2__;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$tag): $mod = ($i % 2 );++$i;?>
            <a href="<?php echo $tag['url']; ?>" class="tag"> <span><?php echo htmlentities($tag['name']); ?></span></a>
            <?php endforeach; endif; else: echo "" ;endif; $__LASTLIST__=$__6N5iGqaXz2__; ?>
        </div>
    </div>
</div>
<!-- E 热门标签 -->

<!-- S 推荐下载 -->
<div class="panel panel-default recommend-article">
    <div class="panel-heading">
        <h3 class="panel-title"><?php echo __('Recommend download'); ?></h3>
    </div>
    <div class="panel-body">
        <?php $__MNnzjXgfC0__ = \addons\cms\model\Archives::getArchivesList(["id"=>"item","model"=>"3","row"=>"10","flag"=>"recommend","orderby"=>"id","orderway"=>"asc"]); if(is_array($__MNnzjXgfC0__) || $__MNnzjXgfC0__ instanceof \think\Collection || $__MNnzjXgfC0__ instanceof \think\Paginator): $i = 0; $__LIST__ = $__MNnzjXgfC0__;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$item): $mod = ($i % 2 );++$i;?>
        <div class="media media-number">
            <div class="media-left">
                <span class="num tag"><?php echo $i; ?></span>
            </div>
            <div class="media-body">
                <a href="<?php echo $item['url']; ?>" title="<?php echo htmlentities($item['title']); ?>"><?php echo htmlentities($item['title']); ?></a>
            </div>
        </div>
        <?php endforeach; endif; else: echo "" ;endif; $__LASTLIST__=$__MNnzjXgfC0__; ?>
    </div>
</div>
<!-- E 推荐下载 -->

<div class="panel panel-blockimg">
    <?php if((!0 || time()>0)&&(!0 || time()<0)): ?><?php echo cache('taglib_cms_block_content_28'); endif; ?>
</div>

        </aside>
    </div>
</div>

</main>

<footer>
    <div id="footer">
        <div class="container">
            <div class="row footer-inner">
                <div class="col-xs-12">
                    <div class="footer-logo pull-left mr-4">
                        <a href="<?php echo addon_url('cms/index/index'); ?>"><i class="fa fa-bookmark"></i></a>
                    </div>
                    <div class="pull-left">
                        Copyright&nbsp;©&nbsp;<?php echo date("Y"); ?> All rights reserved. <?php echo \think\Config::get('cms.sitename'); ?>
                        <a href="https://beian.miit.gov.cn" target="_blank"><?php echo htmlentities($site['beian']); ?></a>

                    <ul class="list-unstyled list-inline mt-2">
                        <li><a href="<?php echo addon_url('cms/page/index', [':diyname'=>'aboutus']); ?>">关于我们</a></li>
                        <li><a href="<?php echo addon_url('cms/page/index', [':diyname'=>'agreement']); ?>">用户协议</a></li>
                        <?php if(config('fastadmin.usercenter')): ?>
                        <li><a href="<?php echo url('index/user/index'); ?>">会员中心</a></li>
                        <?php endif; ?>
                    </ul>
                    </div>

                </div>
            </div>
        </div>
    </div>
</footer>

<div id="floatbtn">
    <!-- S 浮动按钮 -->

    <?php if(isset($config['wxapp'])&&$config['wxapp']): ?>
    <a href="javascript:;">
        <i class="iconfont icon-wxapp"></i>
        <div class="floatbtn-wrapper">
            <div class="qrcode"><img src="<?php echo htmlentities(cdnurl($config['wxapp'])); ?>"></div>
            <p>微信小程序</p>
            <p>微信扫一扫体验</p>
        </div>
    </a>
    <?php endif; if(in_array('postarchives', explode(',', config('cms.usersidenav'))) && config('fastadmin.usercenter')): ?>
    <a class="hover" href="<?php echo url('index/cms.archives/post'); ?>" target="_blank">
        <i class="iconfont icon-pencil"></i>
        <em>立即<br>投稿</em>
    </a>
    <?php endif; ?>

    <div class="floatbtn-item floatbtn-share">
        <i class="iconfont icon-share"></i>
        <div class="floatbtn-wrapper" style="height:50px;top:0">
            <div class="social-share" data-initialized="true" data-mode="prepend">
                <a href="#" class="social-share-icon icon-weibo" target="_blank"></a>
                <a href="#" class="social-share-icon icon-qq" target="_blank"></a>
                <a href="#" class="social-share-icon icon-qzone" target="_blank"></a>
                <a href="#" class="social-share-icon icon-wechat"></a>
            </div>
        </div>
    </div>

    <?php if($config['qrcode']): ?>
    <a href="javascript:;">
        <i class="iconfont icon-qrcode"></i>
        <div class="floatbtn-wrapper">
            <div class="qrcode"><img src="<?php echo htmlentities(cdnurl($config['qrcode'])); ?>"></div>
            <p>微信公众账号</p>
            <p>微信扫一扫加关注</p>
        </div>
    </a>
    <?php endif; if(isset($__ARCHIVES__)): ?>
    <a id="feedback" class="hover" href="#comments">
        <i class="iconfont icon-feedback"></i>
        <em>发表<br>评论</em>
    </a>
    <?php endif; ?>

    <a id="back-to-top" class="hover" href="javascript:;">
        <i class="iconfont icon-backtotop"></i>
        <em>返回<br>顶部</em>
    </a>
    <!-- E 浮动按钮 -->
</div>


<script type="text/javascript" src="/assets/libs/jquery/dist/jquery.min.js?v=<?php echo $site['version']; ?>"></script>
<script type="text/javascript" src="/assets/libs/bootstrap/dist/js/bootstrap.min.js?v=<?php echo $site['version']; ?>"></script>
<script type="text/javascript" src="/assets/libs/fastadmin-layer/dist/layer.js?v=<?php echo $site['version']; ?>"></script>
<script type="text/javascript" src="/assets/libs/art-template/dist/template-native.js?v=<?php echo $site['version']; ?>"></script>
<script type="text/javascript" src="/assets/addons/cms/js/jquery.autocomplete.js?v=<?php echo $site['version']; ?>"></script>
<script type="text/javascript" src="/assets/addons/cms/js/swiper.min.js?v=<?php echo $site['version']; ?>"></script>
<script type="text/javascript" src="/assets/addons/cms/js/share.min.js?v=<?php echo $site['version']; ?>"></script>
<script type="text/javascript" src="/assets/addons/cms/js/cms.js?v=<?php echo $site['version']; ?>"></script>

<?php if($isWechat): ?>
<script type="text/javascript" src="https://res.wx.qq.com/open/js/jweixin-1.6.0.js"></script>
<?php endif; ?>

<script type="text/javascript" src="/assets/addons/cms/js/common.js?v=<?php echo $site['version']; ?>"></script>

{__SCRIPT__}


</body>
</html>
