<?php if (!defined('THINK_PATH')) exit(); /*a:4:{s:85:"/www/wwwroot/zcwl123_com/public/../application/admin/view/department/admin/index.html";i:1681178566;s:67:"/www/wwwroot/zcwl123_com/application/admin/view/layout/default.html";i:1671020443;s:64:"/www/wwwroot/zcwl123_com/application/admin/view/common/meta.html";i:1671020443;s:66:"/www/wwwroot/zcwl123_com/application/admin/view/common/script.html";i:1671020443;}*/ ?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
<title><?php echo (isset($title) && ($title !== '')?$title:''); ?></title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
<meta name="renderer" content="webkit">
<meta name="referrer" content="never">
<meta name="robots" content="noindex, nofollow">

<link rel="shortcut icon" href="/assets/img/favicon.ico" />
<!-- Loading Bootstrap -->
<link href="/assets/css/backend<?php echo \think\Config::get('app_debug')?'':'.min'; ?>.css?v=<?php echo \think\Config::get('site.version'); ?>" rel="stylesheet">

<?php if(\think\Config::get('fastadmin.adminskin')): ?>
<link href="/assets/css/skins/<?php echo \think\Config::get('fastadmin.adminskin'); ?>.css?v=<?php echo \think\Config::get('site.version'); ?>" rel="stylesheet">
<?php endif; ?>

<!-- HTML5 shim, for IE6-8 support of HTML5 elements. All other JS at the end of file. -->
<!--[if lt IE 9]>
  <script src="/assets/js/html5shiv.js"></script>
  <script src="/assets/js/respond.min.js"></script>
<![endif]-->
<script type="text/javascript">
    var require = {
        config:  <?php echo json_encode($config); ?>
    };
</script>

    </head>

    <body class="inside-header inside-aside <?php echo defined('IS_DIALOG') && IS_DIALOG ? 'is-dialog' : ''; ?>">
        <div id="main" role="main">
            <div class="tab-content tab-addtabs">
                <div id="content">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <section class="content-header hide">
                                <h1>
                                    <?php echo __('Dashboard'); ?>
                                    <small><?php echo __('Control panel'); ?></small>
                                </h1>
                            </section>
                            <?php if(!IS_DIALOG && !\think\Config::get('fastadmin.multiplenav') && \think\Config::get('fastadmin.breadcrumb')): ?>
                            <!-- RIBBON -->
                            <div id="ribbon">
                                <ol class="breadcrumb pull-left">
                                    <?php if($auth->check('dashboard')): ?>
                                    <li><a href="dashboard" class="addtabsit"><i class="fa fa-dashboard"></i> <?php echo __('Dashboard'); ?></a></li>
                                    <?php endif; ?>
                                </ol>
                                <ol class="breadcrumb pull-right">
                                    <?php foreach($breadcrumb as $vo): ?>
                                    <li><a href="javascript:;" data-url="<?php echo $vo['url']; ?>"><?php echo $vo['title']; ?></a></li>
                                    <?php endforeach; ?>
                                </ol>
                            </div>
                            <!-- END RIBBON -->
                            <?php endif; ?>
                            <div class="content">
                                
<style>
    .form-commonsearch .form-group {
        margin-left: 0;
        margin-right: 0;
        padding: 0;
    }

    form.form-commonsearch .control-label {
        padding-right: 0;
    }

    .tdtitle {
        margin-bottom: 5px;
        font-weight: 600;
    }

    #channeltree {
        margin-left: -6px;
    }

    #channelbar .panel-heading {
        height: 55px;
        line-height: 25px;
        font-size: 14px;
    }

    @media (max-width: 1230px) {
        .fixed-table-toolbar .search .form-control {
            display: none;
        }
    }

    @media (min-width: 1200px) {

        #channelbar {
            width: 25%;
        }

        #archivespanel {
            width: 75%;
        }
    }

    .archives-label span.label {
        font-weight: normal;
    }

    .archives-title {
        width: 400px;
        overflow: hidden;
        text-overflow: ellipsis;
    }
    .jstree-default .jstree-icon:empty {
        width: 16px;
        height: 20px;
        line-height: 20px;
    }
    .jstree-default .jstree-checkbox {
        background-position: -168px -4px;
    }
    .jstree-default .jstree-themeicon {
        background-position: -265px -4px;
    }
    .jstree-default.jstree-checkbox-selection .jstree-clicked > .jstree-checkbox, .jstree-default .jstree-checked > .jstree-checkbox {
        background-position: -233px -4px;
    }
    .jstree-default .jstree-anchor > .jstree-undetermined {
        background-position: -200px -4px;
    }
    .jstree-default .jstree-checkbox:hover {
        background-position: -168px -36px;
    }
    .jstree-default.jstree-checkbox-selection .jstree-clicked > .jstree-checkbox:hover,
    .jstree-default .jstree-checked > .jstree-checkbox:hover {
        background-position: -233px -36px;
    }
    .jstree-default .jstree-anchor > .jstree-undetermined:hover {
        background-position: -200px -36px;
    }

</style>
<div class="row">
    <div class="col-md-4 hidden-xs hidden-sm" id="channelbar" style="padding-right:0;">
        <div class="panel panel-default panel-intro">
            <div class="panel-heading">
                <div class="panel-lead">
                    <em><?php echo __('Organizational'); ?></em>
                </div>
            </div>
            <div class="panel-body" style="padding: 5px">
                <span class="text-muted"><input type="checkbox" name="" id="checkall"/> <label for="checkall"><small><?php echo __('Check all'); ?></small></label></span>
                <span class="text-muted"><input type="checkbox" name="" id="expandall" checked=""/> <label for="expandall"><small><?php echo __('Expand all'); ?></small></label></span>
                <div id="departmenttree">
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-md-8" id="archivespanel">

        <div class="panel panel-default panel-intro">
            <?php echo build_heading(); ?>

            <div class="panel-body">
                <div id="myTabContent" class="tab-content">
                    <div class="tab-pane fade active in" id="one">
                        <div class="widget-body no-padding">
                            <div id="toolbar" class="toolbar">
                                <?php echo build_toolbar('refresh,add,delete'); ?>
                            </div>
                            <table id="table" class="table table-striped table-bordered table-hover table-nowrap"
                                   data-operate-edit="<?php echo $auth->check('department/admin/edit'); ?>"
                                   data-operate-del="<?php echo $auth->check('department/admin/del'); ?>"
                                   width="100%">
                            </table>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<script id="departmenttpl" type="text/html">
    <div class="">
        <div class="alert alert-warning-light ui-sortable-handle" style="cursor: move;">
            <b><?php echo __('Warning'); ?></b><br>
            <?php echo __('Move tips'); ?>
        </div>
        <!-- /.box-body -->
        <div class="text-black">
            <div class="row">
                <div class="col-sm-12">
                    <select name="channel" class="form-control">
                        <option value="0"><?php echo __('Please select channel'); ?></option>
                        <?php echo $departmentOptions; ?>
                    </select>
                </div>
            </div>
            <!-- /.row -->
        </div>
    </div>
</script>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script src="/assets/js/require<?php echo \think\Config::get('app_debug')?'':'.min'; ?>.js" data-main="/assets/js/require-backend<?php echo \think\Config::get('app_debug')?'':'.min'; ?>.js?v=<?php echo htmlentities($site['version']); ?>"></script>
    </body>
</html>
