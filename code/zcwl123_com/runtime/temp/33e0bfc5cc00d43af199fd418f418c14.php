<?php if (!defined('THINK_PATH')) exit(); /*a:4:{s:58:"/www/wwwroot/zcwl123_com/addons/cms/view/default/user.html";i:1650535601;s:67:"/www/wwwroot/zcwl123_com/addons/cms/view/default/common/layout.html";i:1668416891;s:65:"/www/wwwroot/zcwl123_com/addons/cms/view/default/common/item.html";i:1650535601;s:69:"/www/wwwroot/zcwl123_com/addons/cms/view/default/common/pageinfo.html";i:1631866112;}*/ ?>
<!DOCTYPE html>
<!--[if lt IE 7]>
<html class="lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>
<html class="lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>
<html class="lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class=""> <!--<![endif]-->
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,Chrome=1">
    <meta name="viewport" content="width=device-width,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no">
    <meta name="renderer" content="webkit">
    <title><?php echo htmlentities(\think\Config::get('cms.title')); ?> - <?php echo \think\Config::get('cms.sitename'); ?></title>
    <meta name="keywords" content="<?php echo htmlentities(\think\Config::get('cms.keywords')); ?>"/>
    <meta name="description" content="<?php echo htmlentities(\think\Config::get('cms.description')); ?>"/>

    <link rel="shortcut icon" href="/assets/img/favicon.ico" type="image/x-icon"/>
    <link rel="stylesheet" media="screen" href="/assets/css/bootstrap.min.css?v=<?php echo $site['version']; ?>"/>
    <link rel="stylesheet" media="screen" href="/assets/libs/font-awesome/css/font-awesome.min.css?v=<?php echo $site['version']; ?>"/>
    <link rel="stylesheet" media="screen" href="/assets/libs/fastadmin-layer/dist/theme/default/layer.css?v=<?php echo $site['version']; ?>"/>
    <link rel="stylesheet" media="screen" href="/assets/addons/cms/css/swiper.min.css?v=<?php echo $site['version']; ?>">
    <link rel="stylesheet" media="screen" href="/assets/addons/cms/css/share.min.css?v=<?php echo $site['version']; ?>">
    <link rel="stylesheet" media="screen" href="/assets/addons/cms/css/iconfont.css?v=<?php echo $site['version']; ?>">
    <link rel="stylesheet" media="screen" href="/assets/addons/cms/css/common.css?v=<?php echo $site['version']; ?>"/>

    <!--分享-->
    <meta property="og:title" content="<?php echo htmlentities(\think\Config::get('cms.title')); ?>"/>
    <meta property="og:image" content="<?php echo htmlentities(\think\Config::get('cms.image')); ?>"/>
    <meta property="og:description" content="<?php echo htmlentities(\think\Config::get('cms.description')); ?>"/>

    {__STYLE__}

    <!--[if lt IE 9]>
    <script src="/libs/html5shiv.js"></script>
    <script src="/libs/respond.min.js"></script>
    <![endif]-->

    
</head>
<body class="group-page skin-white">

<header class="header">
    <!-- S 导航 -->
    <nav class="navbar navbar-default navbar-white navbar-fixed-top" role="navigation">
        <div class="container">

            <div class="navbar-header">
                <button type="button" class="navbar-toggle sidebar-toggle">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?php echo \think\Config::get('cms.indexurl'); ?>"><img src="<?php echo cdnurl((\think\Config::get('cms.sitelogo') ?: '/assets/addons/cms/img/logo.png')); ?>" style="height:100%;" alt=""></a>
            </div>

            <div class="collapse navbar-collapse" id="navbar-collapse">
                <ul class="nav navbar-nav" data-current="<?php echo (isset($__CHANNEL__['id']) && ($__CHANNEL__['id'] !== '')?$__CHANNEL__['id']:0); ?>">
                    <!--如果你需要自定义NAV,可使用channellist标签来完成,这里只设置了2级,如果显示无限级,请使用cms:nav标签-->
                    <?php $__Xyi8H6joSl__ = \addons\cms\model\Channel::getChannelList(["id"=>"nav","type"=>"top","condition"=>"1=isnav"]); if(is_array($__Xyi8H6joSl__) || $__Xyi8H6joSl__ instanceof \think\Collection || $__Xyi8H6joSl__ instanceof \think\Paginator): $i = 0; $__LIST__ = $__Xyi8H6joSl__;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$nav): $mod = ($i % 2 );++$i;?>
                    <!--判断是否有子级或高亮当前栏目-->
                    <li class="<?php if($nav['has_child']): ?>dropdown<?php endif; if($nav->is_active): ?> active<?php endif; ?>">
                        <a href="<?php echo $nav['url']; ?>" <?php if($nav['has_child']): ?> data-toggle="dropdown" <?php endif; ?>><?php echo htmlentities($nav['name']); if($nav['has_nav_child']): ?> <b class="caret"></b><?php endif; ?></a>
                        <ul class="dropdown-menu <?php if(!$nav['has_nav_child']): ?>hidden<?php endif; ?>" role="menu">
                            <?php $__XPzIFw4pnu__ = \addons\cms\model\Channel::getChannelList(["id"=>"sub","type"=>"son","typeid"=>$nav['id'],"condition"=>"1=isnav"]); if(is_array($__XPzIFw4pnu__) || $__XPzIFw4pnu__ instanceof \think\Collection || $__XPzIFw4pnu__ instanceof \think\Paginator): $i = 0; $__LIST__ = $__XPzIFw4pnu__;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$sub): $mod = ($i % 2 );++$i;?>
                            <li><a href="<?php echo $sub['url']; ?>"><?php echo htmlentities($sub['name']); ?></a></li>
                            <?php endforeach; endif; else: echo "" ;endif; $__LASTLIST__=$__XPzIFw4pnu__; ?>
                        </ul>
                    </li>
                    <?php endforeach; endif; else: echo "" ;endif; $__LASTLIST__=$__Xyi8H6joSl__; ?>

                    <!--如果需要无限级请使用cms:nav标签-->
                    
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <form class="form-inline navbar-form" action="<?php echo addon_url('cms/search/index'); ?>" method="get">
                            <div class="form-search hidden-sm">
                                <input class="form-control" name="q" data-suggestion-url="<?php echo addon_url('cms/search/suggestion'); ?>" type="search" id="searchinput" value="<?php echo htmlentities((\think\Request::instance()->request('q') ?: '')); ?>" placeholder="搜索">
                                <div class="search-icon"></div>
                            </div>
                            <?php echo token('__searchtoken__'); ?>
                        </form>
                    </li>
                    <?php if(config('fastadmin.usercenter')): ?>
                    <li class="dropdown navbar-userinfo">
                        <?php if($user): ?>
                        <a href="<?php echo url('index/user/index'); ?>" class="dropdown-toggle" data-toggle="dropdown">
                            <span class="avatar-img pull-left"><img src="<?php echo htmlentities(cdnurl($user['avatar'])); ?>" style="width:27px;height:27px;border-radius:50%;" alt=""></span>
                            <span class="visible-xs pull-left ml-2 pt-1"><?php echo htmlentities($user['nickname']); ?> <b class="caret"></b></span>
                        </a>
                        <?php else: ?>
                        <a href="<?php echo url('index/user/index'); ?>" class="dropdown-toggle" data-toggle="dropdown">会员<span class="hidden-sm">中心</span> <b class="caret"></b></a>
                        <?php endif; ?>
                        <ul class="dropdown-menu">
                            <?php if($user): ?>
                            <li><a href="<?php echo url('index/user/index'); ?>"><i class="fa fa-user fa-fw"></i> 会员中心</a></li>
                            <li><a href="<?php echo addon_url('cms/user/index', [':id'=>$user['id']]); ?>"><i class="fa fa-user-circle fa-fw"></i> 我的个人主页</a></li>
                            <?php $sidenav = array_filter(explode(',', config('cms.usersidenav'))); if(in_array('myarchives', $sidenav)): ?>
                            <li><a href="<?php echo url('index/cms.archives/my'); ?>"><i class="fa fa-list fa-fw"></i> 我发布的文档</a></li>
                            <?php endif; if(in_array('postarchives', $sidenav)): ?>
                            <li><a href="<?php echo url('index/cms.archives/post'); ?>"><i class="fa fa-pencil fa-fw"></i> 发布文档</a></li>
                            <?php endif; if(in_array('myorder', $sidenav)): ?>
                            <li><a href="<?php echo url('index/cms.order/index'); ?>"><i class="fa fa-shopping-bag fa-fw"></i> 我的消费订单</a></li>
                            <?php endif; if(in_array('mycomment', $sidenav)): ?>
                            <li><a href="<?php echo url('index/cms.comment/index'); ?>"><i class="fa fa-comments fa-fw"></i> 我发表的评论</a></li>
                            <?php endif; if(in_array('mycollection', $sidenav)): ?>
                            <li><a href="<?php echo url('index/cms.collection/index'); ?>"><i class="fa fa-bookmark fa-fw"></i> 我的收藏</a></li>
                            <?php endif; ?>
                            <li><a href="<?php echo url('index/user/logout'); ?>"><i class="fa fa-sign-out fa-fw"></i> 退出</a></li>
                            <?php else: ?>
                            <li><a href="<?php echo url('index/user/login'); ?>"><i class="fa fa-sign-in fa-fw"></i> 登录</a></li>
                            <li><a href="<?php echo url('index/user/register'); ?>"><i class="fa fa-user-o fa-fw"></i> 注册</a></li>
                            <?php endif; ?>
                        </ul>
                    </li>
                    <?php endif; ?>
                </ul>
            </div>

        </div>
    </nav>
    <!-- E 导航 -->

</header>

<main class="main-content">
    
<style>
    body.skin-white {
        padding-top: 59px;
    }

    @keyframes wave-animation {
        0% {
            transform: translateX(0) translateZ(0) scaleY(1)
        }
        50% {
            transform: translateX(-25%) translateZ(0) scaleY(0.55)
        }
        100% {
            transform: translateX(-50%) translateZ(0) scaleY(1)
        }
    }

    .wave-wrapper {
        height: 430px;
        position: relative;
    }

    .wave-inner {
        position: absolute;
        width: 100%;
        overflow: hidden;
        height: 430px;
        bottom: -1px;
        background: #374486;
    }

    .wave-bg-top {
        z-index: 15;
        opacity: 0.5;
    }

    .wave-bg-middle {
        z-index: 10;
        opacity: 0.75;
    }

    .wave-bg-bottom {
        z-index: 5;
    }

    .wave-item {
        position: absolute;
        left: 0;
        width: 200%;
        height: 430px;
        background-repeat: repeat no-repeat;
        background-position: 0 bottom;
        transform-origin: center bottom;
    }

    .wave-top {
        background-size: 50% 100px;
    }

    .wave-animation .wave-top {
        animation: wave-animation 3s;
        -webkit-animation: wave-animation 3s;
        -webkit-animation-delay: 1s;
        animation-delay: 1s;
    }

    .wave-middle {
        background-size: 50% 120px;
    }

    .wave-animation .wave-middle {
        animation: wave-animation 10s linear infinite;
    }

    .wave-bottom {
        background-size: 50% 100px;
    }

    .wave-animation .wave-bottom {
        animation: wave-animation 15s linear infinite;
    }

    .userinfo {
        position: absolute;
        z-index: 99;
        text-align: center;
        width: 100%;
        top: 0;
        color: #fff;
    }

    .header-logo {
        margin-top: 60px;
        margin-bottom: 20px;
    }

    .userinfo-avatar {
        background: rgba(0, 0, 0, 0.12);
        display: inline-block;
        position: relative;
        padding: 20px;
        border-radius: 50%;
        min-width: 120px;
    }

    .userinfo-avatar img {
        width: 110px;
        height: 110px;
    }

    .nav-noborder > li.active > a, .nav-noborder > li.active > a:focus, .nav-noborder > li.active > a:hover {
        border-color: #fff;
    }

    .nav-noborder {
        border-bottom: none;
    }

    .comment-content {
        border-radius: 3px;
        padding: 12px;
        border: none;
        background: #fafafa;
        margin-bottom: 0;
    }

    .comment-content div {
        line-height: 20px !important;
        margin-bottom: 0px;
        margin-top: 0 !important;
        font-size: 14px !important;
        font-weight: 400;
        color: #777;
        font-family: 'Source Sans Pro', sans-serif;
    }

    .statistics-nums {
        margin-top: 5px;
        color: #999;
    }

    .statistics a:hover {
        text-decoration: none;
    }

    .statistics a:hover .statistics-nums {
        color: #374486;
    }
</style>
<div class="wave-wrapper wave-animation">
    <div class="userinfo">
        <div class="header-logo">
            <a href="<?php echo addon_url('cms/user/index',[':id'=>$__USER__['id']],false); ?>" class="userinfo-avatar">
                <img class="img-circle" src="<?php echo htmlentities(cdnurl($__USER__['avatar'])); ?>" alt="<?php echo htmlentities($__USER__['nickname']); ?>">
            </a>
        </div>
        <h2 class="">
            <span><strong><?php echo htmlentities($__USER__['nickname']); ?></strong></span>
        </h2>
        <h5 class="">
            <span><?php echo htmlentities((isset($__USER__['bio']) && ($__USER__['bio'] !== '')?$__USER__['bio']:"这家伙很懒，什么也没写")); ?></span>
        </h5>
    </div>
    <div class="wave-inner wave-bg-top">
        <div class="wave-item wave-top" style="background-image: url('/assets/addons/cms/img/wave-top.png')"></div>
    </div>
    <div class="wave-inner wave-bg-middle">
        <div class="wave-item wave-middle" style="background-image: url('/assets/addons/cms/img/wave-mid.png')"></div>
    </div>
    <div class="wave-inner wave-bg-bottom">
        <div class="wave-item wave-bottom" style="background-image: url('/assets/addons/cms/img/wave-bot.png')"></div>
    </div>
</div>

<div class="container-fluid" style="padding:0px 0 50px 0;background:#fff;">
    <header class="space-header">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-8 col-md-4 col-md-offset-4">
                    <div class="row statistics pt-1 text-center" style="border-top:none;">
                        <div class="col-xs-4">

                            <div class="statistics-text">文章</div>
                            <a href="<?php echo addon_url('cms/user/index',[':id'=>$__USER__['id']],false); ?>/archives">
                                <div class="statistics-nums"><?php echo $statistics['archives']; ?></div>
                            </a>
                        </div>
                        <div class="col-xs-4">
                            <div class="statistics-text">评论</div>
                            <a href="<?php echo addon_url('cms/user/index',[':id'=>$__USER__['id']],false); ?>/comment">
                                <div class="statistics-nums"><?php echo $statistics['comments']; ?></div>
                            </a>
                        </div>
                        <div class="col-xs-4">
                            <div class="statistics-text">加入时间</div>
                            <div class="statistics-nums"><?php echo human_date($__USER__['jointime']); ?></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
</div>

<div class="container" id="content-container">
    <div class="row mt-4 mb-4">
        <div class="col-md-8 col-sm-12">
            <ul class="nav nav-tabs nav-noborder mb-10 mt-20">
                <li class="active"><a href="javascript:;"><?php echo htmlentities($title); ?></a></li>
            </ul>
            <div class="tab-inner" style="background:#fff;padding:15px;">
                <?php if($type == 'archives'): ?>
                <div class="article-list">
                    <?php if(is_array($archivesList) || $archivesList instanceof \think\Collection || $archivesList instanceof \think\Paginator): $i = 0; $__LIST__ = $archivesList;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$item): $mod = ($i % 2 );++$i;?>
                    <article class="article-item">
    <?php if($item['images'] && count($item['images_list'])>1): ?>
    <div class="gallery-article">
        <h3 class="article-title">
            <a href="<?php echo $item['url']; ?>" <?php if($item['style']): ?>style="<?php echo $item['style_text']; ?>"<?php endif; ?>><?php echo $item->highlight(htmlentities($item->title), $__SEARCHTERM__??''); ?></a>
        </h3>
        <div class="row">
            <?php if(is_array($item['images_list']) || $item['images_list'] instanceof \think\Collection || $item['images_list'] instanceof \think\Paginator): $i = 0;$__LIST__ = is_array($item['images_list']) ? array_slice($item['images_list'],0,4, true) : $item['images_list']->slice(0,4, true); if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$img): $mod = ($i % 2 );++$i;?>
            <div class="col-sm-3 col-xs-6">
                <a href="<?php echo $item['url']; ?>" class="img-zoom">
                    <div class="embed-responsive embed-responsive-4by3">
                        <img src="<?php echo $img; ?>" alt="<?php echo htmlentities($item['title']); ?>" class="embed-responsive-item">
                    </div>
                </a>
            </div>
            <?php endforeach; endif; else: echo "" ;endif; ?>
        </div>
        <div class="media">
            <div class="media-body ml-0">
                <div class="article-intro">
                    <?php echo htmlentities($item['description']); ?>
                </div>
                <div class="article-tag">
                    <a href="<?php echo $item['channel']['url']; ?>" class="tag tag-primary"><?php echo htmlentities($item['channel']['name']); ?></a>
                    <span itemprop="date"><?php echo date("Y年m月d日", $item['publishtime']); ?></span>
                    <span class="hidden-xs" itemprop="likes" title="点赞次数"><i class="fa fa-thumbs-up"></i> <?php echo $item['likes']; ?> 点赞</span>
                    <span class="hidden-xs" itemprop="comments"><a href="<?php echo $item['url']; ?>#comments" target="_blank" title="评论数"><i class="fa fa-comments"></i> <?php echo $item['comments']; ?></a> 评论</span>
                    <span class="hidden-xs" itemprop="views" title="浏览次数"><i class="fa fa-eye"></i> <?php echo $item['views']; ?> 浏览</span>
                </div>
            </div>
        </div>
    </div>
    <?php else: ?>
    <div class="media">
        <div class="media-left">
            <a href="<?php echo $item['url']; ?>">
                <div class="embed-responsive embed-responsive-4by3 img-zoom">
                    <img src="<?php echo $item['image']; ?>" alt="<?php echo htmlentities($item['title']); ?>">
                </div>
            </a>
        </div>
        <div class="media-body">
            <h3 class="article-title">
                <a href="<?php echo $item['url']; ?>" <?php if($item['style']): ?>style="<?php echo $item['style_text']; ?>"<?php endif; ?>><?php echo $item->highlight($item->title, $__SEARCHTERM__??''); ?></a>
            </h3>
            <div class="article-intro">
                <?php echo htmlentities($item['description']); ?>
            </div>
            <div class="article-tag">
                <a href="<?php echo $item['channel']['url']; ?>" class="tag tag-primary"><?php echo htmlentities($item['channel']['name']); ?></a>
                <span itemprop="date"><?php echo date("Y年m月d日", $item['publishtime']); ?></span>
                <span class="hidden-xs" itemprop="likes" title="点赞次数"><i class="fa fa-thumbs-up"></i> <?php echo $item['likes']; ?> 点赞</span>
                <span class="hidden-xs" itemprop="comments"><a href="<?php echo $item['url']; ?>#comments" target="_blank" title="评论数"><i class="fa fa-comments"></i> <?php echo $item['comments']; ?></a> 评论</span>
                <span class="hidden-xs" itemprop="views" title="浏览次数"><i class="fa fa-eye"></i> <?php echo $item['views']; ?> 浏览</span>
            </div>
        </div>
    </div>
    <?php endif; ?>
</article>

                    <?php endforeach; endif; else: echo "" ;endif; ?>
                </div>

                <!--@formatter:off-->
<?php if((config('cms.loadmode')=='paging' && "[loadmode]"!="infinite") || "[loadmode]"=="paging"): ?>
    
    <!-- S 分页栏 -->
    <div class="text-center pager">
        <?php echo $__PAGELIST__->render(['type' => in_array('[type]',['simple', 'full'])?'[type]':config('cms.pagemode')]); ?>
    </div>
    <!-- E 分页栏 -->
    <?php if($__PAGELIST__->isEmpty()): ?>
        <div class="loadmore loadmore-line loadmore-nodata"><span class="loadmore-tips">暂无数据</span></div>
    <?php endif; else: if($__PAGELIST__->isEmpty() || !$__PAGELIST__->hasPages()): if($__PAGELIST__->currentPage()>1 || ($__PAGELIST__->isEmpty() && $__PAGELIST__->currentPage()==1)): ?>
            <div class="loadmore loadmore-line loadmore-nodata"><span class="loadmore-tips">暂无更多数据</span></div>
        <?php endif; else: ?>
        <div class="text-center clearfix">
            <a href="?<?php echo http_build_query(array_merge(request()->get(), ['page'=>$__PAGELIST__->currentPage()+1])); ?>"
               data-url="?<?php echo http_build_query(array_merge(request()->get(), ['page'=>'__page__'])); ?>"
               data-page="<?php echo $__PAGELIST__->currentPage()+1; ?>" class="btn btn-default my-4 px-4 btn-loadmore"
               data-autoload="<?php echo in_array('[autoload]',['true', 'false'])?'[autoload]':'false'; ?>">
                加载更多
            </a>
        </div>
    <?php endif; endif; ?>
<!--@formatter:on-->

                <?php else: ?>
                <div class="article-list">
                    <?php if(is_array($commentList) || $commentList instanceof \think\Collection || $commentList instanceof \think\Paginator): $i = 0; $__LIST__ = $commentList;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$item): $mod = ($i % 2 );++$i;?>
                    <article class="article-item">
                        <div class="media">
                            <div class="media-left">
                                <a href="<?php echo $item['source']['url']; ?>">
                                    <div class="embed-responsive embed-responsive-4by3 img-zoom">
                                        <img src="<?php echo $item['source']['image']; ?>">
                                    </div>
                                </a>
                            </div>
                            <div class="media-body">
                                <h3 class="article-title">
                                    <a href="<?php echo $item['source']['url']; ?>" <?php if(isset($item['source']['style']) && $item['source']['style']): ?>style="<?php echo (isset($item['source']['style_text']) && ($item['source']['style_text'] !== '')?$item['source']['style_text']:''); ?>" <?php endif; ?>><?php echo htmlentities($item['source']['title']); ?></a>
                                </h3>
                                <div class="comment-content">
                                    <div><?php echo $item['content']; ?></div>
                                </div>
                                <div class="article-tag">
                                    <span itemprop="date"><i class="fa fa-calendar"></i> <?php echo date("Y年m月d日", $item['createtime']); ?></span>
                                </div>
                            </div>
                        </div>

                    </article>
                    <?php endforeach; endif; else: echo "" ;endif; ?>
                </div>

                <!--@formatter:off-->
<?php if((config('cms.loadmode')=='paging' && "[loadmode]"!="infinite") || "[loadmode]"=="paging"): ?>
    
    <!-- S 分页栏 -->
    <div class="text-center pager">
        <?php echo $__PAGELIST__->render(['type' => in_array('[type]',['simple', 'full'])?'[type]':config('cms.pagemode')]); ?>
    </div>
    <!-- E 分页栏 -->
    <?php if($__PAGELIST__->isEmpty()): ?>
        <div class="loadmore loadmore-line loadmore-nodata"><span class="loadmore-tips">暂无数据</span></div>
    <?php endif; else: if($__PAGELIST__->isEmpty() || !$__PAGELIST__->hasPages()): if($__PAGELIST__->currentPage()>1 || ($__PAGELIST__->isEmpty() && $__PAGELIST__->currentPage()==1)): ?>
            <div class="loadmore loadmore-line loadmore-nodata"><span class="loadmore-tips">暂无更多数据</span></div>
        <?php endif; else: ?>
        <div class="text-center clearfix">
            <a href="?<?php echo http_build_query(array_merge(request()->get(), ['page'=>$__PAGELIST__->currentPage()+1])); ?>"
               data-url="?<?php echo http_build_query(array_merge(request()->get(), ['page'=>'__page__'])); ?>"
               data-page="<?php echo $__PAGELIST__->currentPage()+1; ?>" class="btn btn-default my-4 px-4 btn-loadmore"
               data-autoload="<?php echo in_array('[autoload]',['true', 'false'])?'[autoload]':'false'; ?>">
                加载更多
            </a>
        </div>
    <?php endif; endif; ?>
<!--@formatter:on-->


                <?php endif; ?>
            </div>
        </div>
        <div class="col-md-4 col-sm-12">
            <div class="article-sidebar">
                <!-- S 会员菜单 -->
                <div class="panel panel-default">
                    <div class="panel-body">
                        <ul class="nav nav-pills nav-stacked space-nav">
                            <li class="<?php echo $type=='archives'?'active':''; ?>"><a href="<?php echo addon_url('cms/user/index',[':id'=>$__USER__['id']],false); ?>/archives"><?php echo $user&&$user['id']==$__USER__['id']?'我':'TA'; ?>的文章</a></li>
                            <li class="<?php echo $type=='comment'?'active':''; ?>"><a href="<?php echo addon_url('cms/user/index',[':id'=>$__USER__['id']],false); ?>/comment"><?php echo $user&&$user['id']==$__USER__['id']?'我':'TA'; ?>的评论</a></li>
                        </ul>
                    </div>
                </div>
                <!-- E 会员菜单 -->

                <div class="panel panel-blockimg">
                    <?php if((!0 || time()>0)&&(!0 || time()<0)): ?><?php echo cache('taglib_cms_block_content_8'); endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>

</main>

<footer>
    <div id="footer">
        <div class="container">
            <div class="row footer-inner">
                <div class="col-xs-12">
                    <div class="footer-logo pull-left mr-4">
                        <a href="<?php echo addon_url('cms/index/index'); ?>"><i class="fa fa-bookmark"></i></a>
                    </div>
                    <div class="pull-left">
                        Copyright&nbsp;©&nbsp;<?php echo date("Y"); ?> All rights reserved. <?php echo \think\Config::get('cms.sitename'); ?>
                        <a href="https://beian.miit.gov.cn" target="_blank"><?php echo htmlentities($site['beian']); ?></a>

                    <ul class="list-unstyled list-inline mt-2">
                        <li><a href="<?php echo addon_url('cms/page/index', [':diyname'=>'aboutus']); ?>">关于我们</a></li>
                        <li><a href="<?php echo addon_url('cms/page/index', [':diyname'=>'agreement']); ?>">用户协议</a></li>
                        <?php if(config('fastadmin.usercenter')): ?>
                        <li><a href="<?php echo url('index/user/index'); ?>">会员中心</a></li>
                        <?php endif; ?>
                    </ul>
                    </div>

                </div>
            </div>
        </div>
    </div>
</footer>

<div id="floatbtn">
    <!-- S 浮动按钮 -->

    <?php if(isset($config['wxapp'])&&$config['wxapp']): ?>
    <a href="javascript:;">
        <i class="iconfont icon-wxapp"></i>
        <div class="floatbtn-wrapper">
            <div class="qrcode"><img src="<?php echo htmlentities(cdnurl($config['wxapp'])); ?>"></div>
            <p>微信小程序</p>
            <p>微信扫一扫体验</p>
        </div>
    </a>
    <?php endif; if(in_array('postarchives', explode(',', config('cms.usersidenav'))) && config('fastadmin.usercenter')): ?>
    <a class="hover" href="<?php echo url('index/cms.archives/post'); ?>" target="_blank">
        <i class="iconfont icon-pencil"></i>
        <em>立即<br>投稿</em>
    </a>
    <?php endif; ?>

    <div class="floatbtn-item floatbtn-share">
        <i class="iconfont icon-share"></i>
        <div class="floatbtn-wrapper" style="height:50px;top:0">
            <div class="social-share" data-initialized="true" data-mode="prepend">
                <a href="#" class="social-share-icon icon-weibo" target="_blank"></a>
                <a href="#" class="social-share-icon icon-qq" target="_blank"></a>
                <a href="#" class="social-share-icon icon-qzone" target="_blank"></a>
                <a href="#" class="social-share-icon icon-wechat"></a>
            </div>
        </div>
    </div>

    <?php if($config['qrcode']): ?>
    <a href="javascript:;">
        <i class="iconfont icon-qrcode"></i>
        <div class="floatbtn-wrapper">
            <div class="qrcode"><img src="<?php echo htmlentities(cdnurl($config['qrcode'])); ?>"></div>
            <p>微信公众账号</p>
            <p>微信扫一扫加关注</p>
        </div>
    </a>
    <?php endif; if(isset($__ARCHIVES__)): ?>
    <a id="feedback" class="hover" href="#comments">
        <i class="iconfont icon-feedback"></i>
        <em>发表<br>评论</em>
    </a>
    <?php endif; ?>

    <a id="back-to-top" class="hover" href="javascript:;">
        <i class="iconfont icon-backtotop"></i>
        <em>返回<br>顶部</em>
    </a>
    <!-- E 浮动按钮 -->
</div>


<script type="text/javascript" src="/assets/libs/jquery/dist/jquery.min.js?v=<?php echo $site['version']; ?>"></script>
<script type="text/javascript" src="/assets/libs/bootstrap/dist/js/bootstrap.min.js?v=<?php echo $site['version']; ?>"></script>
<script type="text/javascript" src="/assets/libs/fastadmin-layer/dist/layer.js?v=<?php echo $site['version']; ?>"></script>
<script type="text/javascript" src="/assets/libs/art-template/dist/template-native.js?v=<?php echo $site['version']; ?>"></script>
<script type="text/javascript" src="/assets/addons/cms/js/jquery.autocomplete.js?v=<?php echo $site['version']; ?>"></script>
<script type="text/javascript" src="/assets/addons/cms/js/swiper.min.js?v=<?php echo $site['version']; ?>"></script>
<script type="text/javascript" src="/assets/addons/cms/js/share.min.js?v=<?php echo $site['version']; ?>"></script>
<script type="text/javascript" src="/assets/addons/cms/js/cms.js?v=<?php echo $site['version']; ?>"></script>

<?php if($isWechat): ?>
<script type="text/javascript" src="https://res.wx.qq.com/open/js/jweixin-1.6.0.js"></script>
<?php endif; ?>

<script type="text/javascript" src="/assets/addons/cms/js/common.js?v=<?php echo $site['version']; ?>"></script>

{__SCRIPT__}


</body>
</html>
