define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'timetable/lovers/index' + location.search,
                    add_url: 'timetable/lovers/add',
                    edit_url: 'timetable/lovers/edit',
                    del_url: 'timetable/lovers/del',
                    multi_url: 'timetable/lovers/multi',
                    import_url: 'timetable/lovers/import',
                    table: 'timetable_lovers',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'lid',
                sortName: 'lid',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'lid', title: __('Lid')},
                        {field: 'fid', title: __('Fid')},
                        {field: 'tid', title: __('Tid')},
                        {field: 'starttime', title: __('Starttime'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'endtime', title: __('Endtime'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'love_sort', title: __('Love_sort'), searchList: {"0":__('Love_sort 0'),"1":__('Love_sort 1'),"2":__('Love_sort 2')}, formatter: Table.api.formatter.normal},
                        {field: 'contents', title: __('Contents'), operate: 'LIKE'},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});