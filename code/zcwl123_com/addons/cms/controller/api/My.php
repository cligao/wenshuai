<?php

namespace addons\cms\controller\api;

use addons\cms\model\Comment;
use addons\cms\model\Page;
use think\Response;
use think\Db;


/**
 * 我的
 */
class My extends Base
{
    protected $noNeedLogin = ['aboutus', 'agreement'];

    /**
     * 我发表的评论
     */
    public function comment()
    {
        $commentList = Comment::with('archives')
            ->where(['user_id' => $this->auth->id])
            ->where(['type' => 'archives'])
            ->where(['status' => 'normal'])
            ->order('id desc')
            ->field('id,aid,type,pid,content,createtime')
            ->paginate();

        foreach ($commentList as $index => $item) {
            $item->create_date = human_date($item->createtime);
            if ($item->archives) {
                $item->aid = $item->archives->eid;
                $item->archives->eid = $item->archives->eid;
            }
        }
        $commentList = $commentList->toArray();

        foreach ($commentList['data'] as $index => &$datum) {
            if (isset($datum['archives']) && $datum['archives']) {
                $datum['archives']['id'] = $datum['archives']['eid'];
                unset($datum['archives']['channel']);
            }
        }

        $this->success('', ['commentList' => $commentList]);
    }

    public function kcmyz()
    {
        $kcm = $this->request->param('kcm');
        $userid = $this->auth->id;

        $kcmuser = Db::name('cms_kechengma')->where('kechengma', $kcm)->find();
        if($kcmuser == NULL){
            $this->error(__('没有该课程码'), null, ['status'=>1]);
        } else {
            $used = Db::name('cms_kechengma')->where('kechengma', $kcm)->where('usedid',$userid)->find();
            if(!empty($used)){
                $this->error(__('课程码已使用'), null, ['status'=>2]);
            } else {

                $ok = false;
                $par = $this->getparent($userid);

                if(in_array($kcmuser['user_id'], $par)){
                    $ok = true;
                }

                if($ok){
                    Db::name('cms_kechengma')->where('kechengma', $kcm)->update(['usedid'=>$userid]);
                    Db::name('user')->where('id',$userid)->update(['youquan'=>1]);

                    $fxjl = config('site.fxjl');
                    $uinfo = Db::name('user')->where('id', $userid)->field('pid')->find();
                    $pinfo = Db::name('user')->where('id', $uinfo['pid'])->field('money,shenfen')->find();
                    if($pinfo['shenfen'] != 'hhr' && $pinfo['shenfen'] != 'tz'){
                        $ilog = [];
                        $ilog['user_id'] = $uinfo['pid'];
                        $ilog['money'] = $fxjl;
                        $ilog['before'] = $pinfo['money'];
                        $ilog['after'] = $pinfo['money'] + $fxjl;
                        $ilog['memo'] = '使用课程码奖励用户'.$uinfo['pid'].$fxjl.'元';
                        $ilog['createtime'] = time();
                        Db::name('user_money_log')->insert($ilog);
                        Db::name('user')->where('id', $uinfo['pid'])->update(['money' => Db::raw('money+'.$fxjl)]);
                    }

                    $this->success('课程码核销成功！', ['status'=>3]);
                }else{
                    $this->error(__('没有使用权限'), null, ['status'=>3]);
                }
            }
        }
    }

    public function getteam(){

        $id = $this->request->request('usid');
        $usid = $id ? $id : $this->auth->id;

        $list = \app\common\model\User::where(['pid' => $usid])
            ->order('id desc')
            ->paginate(10);

        foreach ($list as $v){
            $v->joins = date('Y-m-d', $v->jointime);
            if($v->wxname){
                $v->wxname = $v->wxname;
            }else{
                $v->wxname = $v->mobile;
            }
            $hhrt = $this->hhrchild($v->id);
            $hhrcount = count($hhrt);
            $v->threeteam = $hhrcount;

        }

        $this->success('', $list);
    }

    public function gettixian(){
        $money = $this->request->param('money');
        $account = $this->request->param('zfb');
        $name = $this->request->param('username');

        $type = 'alipay';

        Db::startTrans();
        try {
            $data = [
                'orderid' => date("YmdHis") . sprintf("%08d", $this->auth->id) . mt_rand(1000, 9999),
                'user_id' => $this->auth->id,
                'money'   => $money,
                'type'    => $type,
                'account' => $account,
                'name'    => $name,
            ];
            \addons\withdraw\model\Withdraw::create($data);
            \app\common\model\User::money(-$money, $this->auth->id, "提现");
            Db::commit();
        } catch (Exception $e) {
            Db::rollback();
            $this->error($e->getMessage());
        }
        $this->success("提现申请成功！请等待后台审核！", []);
        return;


    }

    public function teaminfo(){
        $userid = $this->auth->id;
        $oneteam = $this->childteam($userid);
        $teamcount = count($oneteam);

        $hhrteam = $this->hhrchild($userid);
        $hhrteamcount = count($hhrteam);

        $ptteam = $this->ptchild($userid);
        $ptteamcount = count($ptteam);

        $dayteam = $this->daymem($userid);
        $daycount = count($dayteam);

        $data['daycount'] = $daycount ? $daycount : 0;
        $data['ptcount'] = $ptteamcount ? $ptteamcount : 0;
        $data['hhrcount'] = $hhrteamcount ? $hhrteamcount : 0;
        $data['teamcount'] = $teamcount ? $teamcount : 0;
        $this->success('', ['data' => $data]);
    }

    function daymem($id){
        $daystart = strtotime('Y-m-d 00:00:00');
        $dayend = strtotime('Y-m-d 23:59:59');
        static $chp = [];
        $childs = \app\common\model\User::where('pid', $id)->where('jointime','>',$daystart)->where('jointime','<',$dayend)->select();
        foreach($childs as $v){
            $chp[] = $v->id;
            $this->daymem($v->id);
        }

        return $chp;
    }

    function ptchild($id){
        static $chp = [];
        $childs = \app\common\model\User::where('pid', $id)->select();
        foreach($childs as $v){
            if($v->shenfen == ''){
                $chp[] = $v->id;
                $this->ptchild($v->id);
            }
        }

        return $chp;
    }

    function hhrchild($id){
        static $chp = [];
        $childs = \app\common\model\User::where('pid', $id)->select();
        foreach($childs as $v){
            if($v->shenfen == 'hhr'){
                $chp[] = $v->id;
                $this->hhrchild($v->id);
            }
        }

        return $chp;
    }

    function childteam($id){
        static $chp = [];
        $childs = \app\common\model\User::where('pid', $id)->select();
        foreach($childs as $v){
            $chp[] = $v->id;
            $this->childteam($v->id);
        }

        return $chp;
    }

    function getparent($id){
        static $paras = [];
        $para = Db::name('user')->where('id', $id)->find();
        if($para['pid']){
            $paras[] = $para['pid'];
            $this->getparent($para['pid']);
        }

        return $paras;
    }

    public function getkecheng()
    {
        $info = $this->auth->getUserInfo();
        if($info['shenfen'] != 'hhr' && $info['shenfen'] != 'tz'){
            $this->error(__('不是合伙人'));
        }else{
            $ishave = Db::name('cms_kechengma')->where('user_id', $info['id'])->whereNull('usedid')->count();
            if($ishave <= 0){
                for($i=0;$i<10;$i++){
                    $ma[$i] = rand(10000000,99999999);
                }
                foreach($ma as $k=>$v){
                    $data = [];
                    $data['user_id'] = $info['id'];
                    $data['kechengma'] = $v;
                    $data['createtime'] = time();
                    Db::name('cms_kechengma')->insert($data);
                }

                $this->success('创建成功！', []);
            } else {
                $this->error(__('请使用现有课程码'));
            }
        }
    }

    public function getkechengma()
    {
        $kechenglist = Db::name('cms_kechengma')->where('user_id', $this->auth->id)->whereNull('usedid')
            ->order('id', 'desc')
            ->paginate(10, null);

        $kechenglist = $kechenglist->toArray();

        foreach ($kechenglist['data'] as $k => $v) {
            $kechenglist['data'][$k]['createtime'] = date('Y-m-d H:i:s', $v['createtime']);
        }

        $this->success('', ['orderList' => $kechenglist]);
    }

    /**
     * 我的消费订单
     */
    public function order()
    {
        $orderList = \addons\cms\model\Order::field('id,amount,archives_id,title,payamount,createtime')->with([
            'archives' => function ($query) {
                $query->field('id,user_id,title,channel_id,dislikes,likes,tags,createtime,image,images,style,comments,views,diyname');
            }
        ])->where('user_id', $this->auth->id)
            ->where('status', 'paid')
            ->order('id', 'desc')
            ->paginate(10, null);

        foreach ($orderList as $item) {
            $item->createtime = date('Y-m-d H:i:s', $item->createtime);
            if ($item->archives) {
                $item->archives_id = $item->archives->eid;
                $item->archives->eid = $item->archives->eid;
            }
        }
        $orderList = $orderList->toArray();

        foreach ($orderList['data'] as $index => &$datum) {
            if (isset($datum['archives']) && $datum['archives']) {
                $datum['archives']['id'] = $datum['archives']['eid'];
                unset($datum['archives']['channel']);
            }
            unset($datum['id']);
        }

        $this->success('', ['orderList' => $orderList]);
    }

    /**
     * 关于我们
     */
    public function aboutus()
    {
        $pageInfo = $this->getPage('aboutus');
        $this->success('', ['pageInfo' => $pageInfo]);
    }

    public function haibao(){

        $config = get_addon_config('qrcode');
        $protocol = 'http://';

        if ((!empty($_SERVER['HTTPS']) && 'off' !== $_SERVER['HTTPS']) || ($_SERVER['HTTP_X_FORWARDED_PROTO'] ?? 'http') === 'https') {
            $protocol = 'https://';
        }
    
        $urls = $protocol.$_SERVER['HTTP_HOST'].'/pages/#/?shareid='.$this->auth->id;

        $params['text'] = $urls;
        $params['label'] = $this->request->get('label', $config['label'], 'trim');

        $qrCode = \addons\qrcode\library\Service::qrcode($params);

        $mimetype = $config['format'] == 'png' ? 'image/png' : 'image/svg+xml';

        $response = Response::create()->header("Content-Type", $mimetype);

        // 直接显示二维码
        header('Content-Type: ' . $qrCode->getContentType());
        $response->content($qrCode->writeString());

        // 写入到文件
        if ($config['writefile']) {
            $qrcodePath = ROOT_PATH . 'public/uploads/qrcode/';
            if (!is_dir($qrcodePath)) {
                @mkdir($qrcodePath);
            }
            if (is_really_writable($qrcodePath)) {
                $filePath = $qrcodePath . md5(implode('', $params)) . '.' . $config['format'];
                $qrCode->writeFile($filePath);
            }
        }

        $newbgimg = time() . rand(100000, 999999) . rand(100, 999) . '.jpg';

        copy($qrcodePath . '/bj.jpg', $qrcodePath . $newbgimg);

        $echo_path = $qrcodePath . $newbgimg;
        $background = imagecreatefromstring(file_get_contents($echo_path));
        $qrcode_res = imagecreatefromstring(file_get_contents($filePath));

        list($src_w, $src_h) = getimagesize($filePath);
        imagecopyresampled($background, $qrcode_res, 350, 840, 0, 0, 230, 230, $src_w, $src_h); //修改二维码位置

        imagepng($background, $echo_path);    
        imagedestroy($background);
        imagedestroy($qrcode_res);

        $qrcode = $protocol.$_SERVER['HTTP_HOST'].'/uploads/qrcode/'.$newbgimg;

        $pageInfo = $this->getPage('aboutus');
        $this->success('', ['pageInfo' => $pageInfo, 'qrcode'=>$qrcode]);
    }

    /**
     * 用户协议
     */
    public function agreement()
    {
        $pageInfo = $this->getPage('agreement');
        $this->success('', ['pageInfo' => $pageInfo]);
    }

    /**
     * 获取指定单页
     * @param $name
     * @return mixed
     */
    protected function getPage($name)
    {
        $pageInfo = Page::getByDiyname($name);
        if (!$pageInfo || $pageInfo['status'] != 'normal') {
            $this->error(__('单页未找到'));
        }
        unset($pageInfo['status'], $pageInfo['showtpl'], $pageInfo['parsetpl']);
        $view = new \think\View();
        $pageInfo['content'] = $view->fetch($pageInfo['content'], ["__PAGE__" => $pageInfo], [], [], true);
        return $pageInfo->toArray();
    }
}
