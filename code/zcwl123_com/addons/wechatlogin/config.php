<?php

return [
    [
        'name'    => 'onlywechat',
        'title'   => '前台微信登陆',
        'type'    => 'radio',
        'content' => [
            1 => '通过',
            0 => '不通过',
        ],
        'value'   => '1',
        'rule'    => 'required',
        'msg'     => '',
        'tip'     => '用于配置前台只通过微信登陆。',
        'ok'      => '',
        'extend'  => ''
    ],
    [
        'name'    => 'ser_cli',
        'title'   => '是否主接微信',
        'type'    => 'radio',
        'content' => [
            1 => '直接对接公众号',
            0 => '间接对接公众号',
        ],
        'value'   => '1',
        'rule'    => 'required',
        'msg'     => '',
        'tip'     => '跳转登陆的主次',
        'ok'      => '',
        'extend'  => ''
    ],
    [
        'name'    => 'serve_path',
        'title'   => '主服务器地址',
        'type'    => 'string',
        'content' => [
        ],
        'value'   => 'http://www.xxxx.cn',
        'rule'    => 'required',
        'msg'     => '',
        'tip'     => '直接对接微信公众号的demain',
        'ok'      => '',
        'extend'  => ''
    ],
    [
        'name'    => 'password',
        'title'   => '主从通讯密码',
        'type'    => 'string',
        'content' => [
        ],
        'value'   => 'XXxx00sdsad',
        'rule'    => 'required',
        'msg'     => '',
        'tip'     => '主从服务器之间通信的密码',
        'ok'      => '',
        'extend'  => ''
    ],
    [
        'name'    => '__tips__',
        'title'   => '插件说明',
        'type'    => 'string',
        'content' => [
        ],
        'value'   => '1.本插件的配置用例说明：<a href="https://shimo.im/docs/wn7sGBydyA8E4ZEr/" target="_blank">点击查看详情</a><br>
                      2.本插件针对一个微信公众号，对接多个fa平台，达到跳转自动登录的功能。<br>
                      3.设置，直接对接公众号：请在次平台下的 第三方登陆插件中设置微信的app_id、app_secret、scope=snsapi_userinfo<br>
                      4.设置，间接对接公众号：需要安装第三方登陆插件，而不需要设置第三方登陆插件。且插件设置中填写主服务器地址:<b>http://www.XXXX.com</b><br>
                      ',
        'rule'    => '',
        'msg'     => '',
        'tip'     => '',
        'ok'      => '',
        'extend'  => ''
    ],
];
