<?php

namespace addons\timetable\controller\api;

use app\common\controller\Api;
use think\addons\Controller;
use think\Db;
use think\Exception;
use think\exception\PDOException;

class Searchsq extends Controller
{

    protected $noNeedLogin = [];

    public function index(){
        $keyword = $this->request->post('sq');
        $ishave = Db::name('timetable_subjects')->where('wxname', $keyword)->whereOr('mobile', $keyword)->find();

        $this->success('','',$ishave);
        exit;
    }

}
