export default {
    color: {
        primary: "#5E99FB",
    },
    gradeList: [
        { label: "", value: 0 },
        { label: "大一", value: 1 },
        { label: "大二", value: 2 },
        { label: "大三", value: 3 },
        { label: "大四", value: 4 },
        { label: "研一", value: 5 },
        { label: "研二", value: 6 },
        { label: "研三", value: 7 },
    ],
    colorList: [
        "#08D8D1",
        "#FFD258",
        "#FF8771",
        "#5E99FB",
        "#12DB7E",
        "#9069F8",
        "#FA82DA",
        "#FFAE5D",
    ],
    siteId: "0",
    cryptoKey: "ca6dbbe3333a079b",
    server: {
        api: {
            baseUrl: "https://timetable.6vapp.com/api/timetable",
        },
        img: {
            baseUrl: "https://timetable.6vapp.com/uploads/timetable/bgimg/0/",
        },
    },
};
