<?php

namespace addons\vip\controller\api;

use addons\third\model\Third;
use addons\vip\library\Service;
use addons\vip\model\Record;
use addons\vip\model\Vip;
use fast\Date;
use think\Db;
use think\Exception;
use think\exception\PDOException;

class Order extends Base
{

    protected $noNeedLogin = ['epay'];

    /**
     * 创建订单并发起支付请求
     */
    public function submit()
    {
        //zebraline vip和广告支付都到这里
        $level = $this->request->param('level/d');
        $days = $this->request->param('days/d');
        $viptype = $this->request->param('viptype') ? $this->request->param('viptype') : '';
        $types = $this->request->param('types');
        
        $paytype = $this->request->param('paytype', '');
        $method = $this->request->param('method');
        $appid = $this->request->param('appid');//APP的应用ID
        $returnurl = $this->request->param('returnurl', '', 'trim');

        $vipInfo = Vip::getByLevel($level);
        if (!$vipInfo) {
            $this->error('未找到VIP相关信息');
        }
        if ($this->auth->vip > $vipInfo['level']) {
            $this->error('当前VIP等级已高于购买的VIP等级');
        }
        if (!in_array($paytype, ['alipay', 'wechat'])) {
            $this->error('支付方式错误');
        }
        $amount = $vipInfo->getPriceByDays($days);
        $insert = [
            'user_id' => $this->auth->id,
            'vip_id'  => $vipInfo->id,
            'level'   => $vipInfo->level,
            'viptype' => $viptype,
            'days'    => $days,
            'amount'  => $amount,
            'status'  => 'created',
        ];
        $vipRecord = Record::create($insert, true);

        //公众号和小程序
        $openid = '';
        if (in_array($method, ['miniapp', 'mp'])) {
            $third = Third::where('platform', 'wechat')->where('apptype', $method)->where('user_id', $this->auth->id)->find();
            if (!$third) {
                $this->error("未找到登录用户信息", 'bind');
            }
            $openid = $third['openid'];
        }
        
        try {
            $response = \addons\vip\library\Order::submit($vipInfo->id, $vipRecord->id, $amount, $paytype, $method, $openid, '', $returnurl, $viptype, $types);
        } catch (\Exception $e) {
            $this->error($e->getMessage());
        }

        $this->success(__(''), $response);
    }

    public function rechargesubmit(){
        $amount = $this->request->param('num');
        $paytype = $this->request->param('paytype', '');
        $method = $this->request->param('method');
        $appid = $this->request->param('appid');//APP的应用ID
        $returnurl = $this->request->param('returnurl', '', 'trim');

        
        if (!in_array($paytype, ['alipay', 'wechat'])) {
            $this->error('支付方式错误');
        }
        
        //公众号和小程序
        $openid = '';
        if (in_array($method, ['miniapp', 'mp'])) {
            $third = Third::where('platform', 'wechat')->where('apptype', $method)->where('user_id', $this->auth->id)->find();
            if (!$third) {
                $this->error("未找到登录用户信息", 'bind');
            }
            $openid = $third['openid'];
        }

        try {
            $response = \addons\vip\library\Order::submits($amount, $paytype, $method, $openid, '', $returnurl);
        } catch (\Exception $e) {
            $this->error($e->getMessage());
        }

        $this->success(__(''), $response);
    }

}
