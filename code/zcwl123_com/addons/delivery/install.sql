CREATE TABLE IF NOT EXISTS `__PREFIX__delivery_cat` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `name` varchar(255) NOT NULL COMMENT '分类名称',
    PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COMMENT='商品分类';

CREATE TABLE IF NOT EXISTS `__PREFIX__delivery_express` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `name` varchar(255) NOT NULL DEFAULT '',
    `code` varchar(255) NOT NULL DEFAULT '',
    `sort` int(11) NOT NULL DEFAULT '100',
    `type` varchar(255) NOT NULL DEFAULT '' COMMENT '数据类型：kdniao=快递鸟',
    `is_delete` smallint(1) NOT NULL DEFAULT '0',
    PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=103 DEFAULT CHARSET=utf8mb4 COMMENT='快递公司';

CREATE TABLE IF NOT EXISTS `__PREFIX__delivery_express_bill` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `express_id` int(11) DEFAULT NULL COMMENT '快递公司id',
    `customer_name` varchar(255) DEFAULT NULL COMMENT '电子面单客户账号',
    `customer_pwd` varchar(255) DEFAULT NULL COMMENT '电子面单密码',
    `month_code` varchar(255) DEFAULT NULL COMMENT '月结编码',
    `send_site` varchar(255) DEFAULT NULL COMMENT '网点编码',
    `send_name` varchar(255) DEFAULT NULL COMMENT '网点名称',
    `deletetime` int(11) unsigned DEFAULT NULL,
    `addtime` int(11) DEFAULT NULL,
    PRIMARY KEY (`id`)
    ) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COMMENT='快递单';
CREATE TABLE IF NOT EXISTS `__PREFIX__delivery_express_bill_sender` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `company` varchar(255) DEFAULT NULL,
    `name` varchar(255) DEFAULT NULL,
    `tel` varchar(255) DEFAULT NULL,
    `mobile` varchar(255) DEFAULT NULL,
    `post_code` varchar(255) DEFAULT NULL,
    `province` varchar(255) DEFAULT NULL,
    `city` varchar(255) DEFAULT NULL,
    `exp_area` varchar(255) DEFAULT NULL,
    `address` varchar(255) DEFAULT NULL,
    `deletetime` int(11) DEFAULT NULL,
    `addtime` int(11) DEFAULT NULL,
    `delivery_id` int(11) DEFAULT NULL,
    PRIMARY KEY (`id`)
    ) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4;
CREATE TABLE IF NOT EXISTS `__PREFIX__delivery_free_delivery_rules` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `price` decimal(10,2) DEFAULT NULL,
    `city` longtext,
    `deletetime` int(11) unsigned DEFAULT NULL,
    `addtime` int(11) unsigned DEFAULT NULL,
    PRIMARY KEY (`id`)
    ) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb4;
CREATE TABLE IF NOT EXISTS `__PREFIX__delivery_goods` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `name` varchar(255) NOT NULL COMMENT '商品名称',
    `price` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '售价',
    `cat_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '商品类别',
    `addtime` int(11) NOT NULL DEFAULT '0',
    `deletetime` int(11) unsigned DEFAULT NULL,
    `sort` int(11) NOT NULL DEFAULT '1000' COMMENT '排序  升序',
    `cimage` varchar(1000) DEFAULT NULL COMMENT '商品缩略图',
    `freight` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '运费模板ID',
    `cimages` text,
    `weight` double(10,2) DEFAULT '0.00' COMMENT '重量',
    PRIMARY KEY (`id`),
    KEY `cat_id` (`cat_id`)
    ) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COMMENT='发货商品表';
CREATE TABLE IF NOT EXISTS `__PREFIX__delivery_order` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `order_no` varchar(255) NOT NULL COMMENT '订单号',
    `total_price` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '订单总费用(包含运费）',
    `goods_price` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '商品总价',
    `express_price` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '运费',
    `accept_mobile` varchar(255) DEFAULT NULL COMMENT '收货人手机',
    `accept_name` varchar(255) DEFAULT NULL COMMENT '收货人姓名',
    `remark` varchar(1000) NOT NULL DEFAULT '' COMMENT '订单备注',
    `is_pay` smallint(6) NOT NULL DEFAULT '0' COMMENT '支付状态：0=未支付，1=已支付',
    `pay_type` smallint(6) NOT NULL DEFAULT '0' COMMENT '支付方式：1=微信支付2=支付宝3=现金4=银行卡',
    `pay_time` int(11) NOT NULL DEFAULT '0' COMMENT '支付时间',
    `is_send` smallint(1) NOT NULL DEFAULT '0' COMMENT '发货状态：0=未发货，1=已发货',
    `send_time` int(11) NOT NULL DEFAULT '0' COMMENT '发货时间',
    `express` varchar(255) NOT NULL DEFAULT '' COMMENT '物流公司',
    `express_no` varchar(255) NOT NULL DEFAULT '',
    `addtime` int(11) NOT NULL DEFAULT '0',
    `content` longtext,
    `seller_comments` mediumtext COMMENT '商家备注',
    `deletetime` int(11) unsigned DEFAULT NULL COMMENT '删除时间',
    `accept_province` varchar(255) DEFAULT NULL,
    `accept_city` varchar(255) DEFAULT NULL,
    `accept_exp_area` varchar(255) DEFAULT NULL,
    `accept_address` varchar(1000) DEFAULT NULL COMMENT '收货详细地址',
    `send_province` varchar(255) DEFAULT NULL,
    `send_city` varchar(255) DEFAULT NULL,
    `send_exp_area` varchar(255) DEFAULT NULL,
    `send_address` varchar(255) DEFAULT NULL,
    `send_company` varchar(255) DEFAULT NULL,
    `send_name` varchar(255) DEFAULT NULL,
    `send_tel` varchar(255) DEFAULT NULL,
    `send_mobile` varchar(255) DEFAULT NULL,
    `post_code` varchar(255) DEFAULT NULL,
    `is_confirm` tinyint(1) DEFAULT '0',
    `words` varchar(1000) DEFAULT '' COMMENT '发货备注',
    `accept_post_code` varchar(255) DEFAULT '' COMMENT '收件人邮编',
    PRIMARY KEY (`id`),
    KEY `addtime` (`addtime`) USING BTREE,
    KEY `order_no` (`order_no`(191)) USING BTREE
    ) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COMMENT='订单表';
CREATE TABLE IF NOT EXISTS `__PREFIX__delivery_order_detail` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `order_id` int(11) NOT NULL,
    `goods_id` int(11) NOT NULL,
    `num` int(11) NOT NULL DEFAULT '1' COMMENT '商品数量',
    `total_price` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '此商品的总价',
    `addtime` int(11) NOT NULL DEFAULT '0',
    `deletetime` int(11) unsigned DEFAULT NULL,
    `price` decimal(10,2) DEFAULT '0.00' COMMENT '单价',
    `cimage` varchar(255) DEFAULT NULL,
    `name` varchar(255) DEFAULT NULL,
    `freight` int(11) unsigned DEFAULT '0' COMMENT '运费模板ID',
    `weight` double(10,2) DEFAULT '0.00' COMMENT '重量',
    PRIMARY KEY (`id`),
    KEY `order_id` (`order_id`),
    KEY `goods_id` (`goods_id`),
    KEY `addtime` (`addtime`)
    ) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4 COMMENT='订单明细';
CREATE TABLE IF NOT EXISTS `__PREFIX__delivery_postage_rules` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `name` varchar(255) NOT NULL COMMENT '名称',
    `express_id` int(11) DEFAULT '0' COMMENT '物流公司',
    `detail` longtext NOT NULL COMMENT '规则详细',
    `addtime` int(11) NOT NULL DEFAULT '0',
    `is_default` smallint(1) NOT NULL DEFAULT '0' COMMENT '是否默认：0=否，1=是',
    `deletetime` int(11) DEFAULT NULL,
    `express` varchar(255) NOT NULL DEFAULT '' COMMENT '快递公司',
    `type` smallint(1) unsigned NOT NULL DEFAULT '1' COMMENT '计费方式【1=>按重计费、2=>按件计费】',
    PRIMARY KEY (`id`)
    ) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8mb4 COMMENT='运费规则';
CREATE TABLE IF NOT EXISTS `__PREFIX__delivery_set` (
   `id` int(11) NOT NULL AUTO_INCREMENT,
   `kdniao_mch_id` varchar(255) NOT NULL DEFAULT '' COMMENT '快递鸟商户号',
   `kdniao_api_key` varchar(255) DEFAULT '' COMMENT '快递鸟api key',
   PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COMMENT='发货设置表';
INSERT INTO `__PREFIX__delivery_set` VALUES ('1', 'test', '');


INSERT INTO `__PREFIX__delivery_express` VALUES ('1', '顺丰快递', 'SF', '1', 'kdniao', '0');
INSERT INTO `__PREFIX__delivery_express` VALUES ('2', '申通快递', 'STO', '1', 'kdniao', '0');
INSERT INTO `__PREFIX__delivery_express` VALUES ('3', '韵达快递', 'YD', '1', 'kdniao', '0');
INSERT INTO `__PREFIX__delivery_express` VALUES ('4', '圆通速递', 'YTO', '1', 'kdniao', '0');
INSERT INTO `__PREFIX__delivery_express` VALUES ('5', '中通速递', 'ZTO', '1', 'kdniao', '0');
INSERT INTO `__PREFIX__delivery_express` VALUES ('6', '百世快递', 'HTKY', '1', 'kdniao', '0');
INSERT INTO `__PREFIX__delivery_express` VALUES ('7', 'EMS', 'EMS', '2', 'kdniao', '0');
INSERT INTO `__PREFIX__delivery_express` VALUES ('8', '天天快递', 'HHTT', '2', 'kdniao', '0');
INSERT INTO `__PREFIX__delivery_express` VALUES ('9', '邮政平邮/小包', 'YZPY', '2', 'kdniao', '0');
INSERT INTO `__PREFIX__delivery_express` VALUES ('10', '宅急送', 'ZJS', '2', 'kdniao', '0');
INSERT INTO `__PREFIX__delivery_express` VALUES ('11', '国通快递', 'GTO', '5', 'kdniao', '0');
INSERT INTO `__PREFIX__delivery_express` VALUES ('12', '全峰快递', 'QFKD', '5', 'kdniao', '0');
INSERT INTO `__PREFIX__delivery_express` VALUES ('13', '优速快递', 'UC', '5', 'kdniao', '0');
INSERT INTO `__PREFIX__delivery_express` VALUES ('14', '中铁快运', 'ZTKY', '5', 'kdniao', '0');
INSERT INTO `__PREFIX__delivery_express` VALUES ('15', '中铁物流', 'ZTWL', '5', 'kdniao', '0');
INSERT INTO `__PREFIX__delivery_express` VALUES ('16', '亚马逊物流', 'AMAZON', '5', 'kdniao', '0');
INSERT INTO `__PREFIX__delivery_express` VALUES ('17', '城际快递', 'CJKD', '5', 'kdniao', '0');
INSERT INTO `__PREFIX__delivery_express` VALUES ('18', '德邦', 'DBL', '5', 'kdniao', '0');
INSERT INTO `__PREFIX__delivery_express` VALUES ('19', '汇丰物流', 'HFWL', '5', 'kdniao', '0');
INSERT INTO `__PREFIX__delivery_express` VALUES ('20', '百世快运', 'BTWL', '100', 'kdniao', '0');
INSERT INTO `__PREFIX__delivery_express` VALUES ('21', '安捷快递', 'AJ', '100', 'kdniao', '0');
INSERT INTO `__PREFIX__delivery_express` VALUES ('22', '安能物流', 'ANE', '100', 'kdniao', '0');
INSERT INTO `__PREFIX__delivery_express` VALUES ('23', '安信达快递', 'AXD', '100', 'kdniao', '0');
INSERT INTO `__PREFIX__delivery_express` VALUES ('24', '北青小红帽', 'BQXHM', '100', 'kdniao', '0');
INSERT INTO `__PREFIX__delivery_express` VALUES ('25', '百福东方', 'BFDF', '100', 'kdniao', '0');
INSERT INTO `__PREFIX__delivery_express` VALUES ('26', 'CCES快递', 'CCES', '100', 'kdniao', '0');
INSERT INTO `__PREFIX__delivery_express` VALUES ('27', '城市100', 'CITY100', '100', 'kdniao', '0');
INSERT INTO `__PREFIX__delivery_express` VALUES ('28', 'COE东方快递', 'COE', '100', 'kdniao', '0');
INSERT INTO `__PREFIX__delivery_express` VALUES ('29', '长沙创一', 'CSCY', '100', 'kdniao', '0');
INSERT INTO `__PREFIX__delivery_express` VALUES ('30', '成都善途速运', 'CDSTKY', '100', 'kdniao', '0');
INSERT INTO `__PREFIX__delivery_express` VALUES ('31', 'D速物流', 'DSWL', '100', 'kdniao', '0');
INSERT INTO `__PREFIX__delivery_express` VALUES ('32', '大田物流', 'DTWL', '100', 'kdniao', '0');
INSERT INTO `__PREFIX__delivery_express` VALUES ('33', '快捷速递', 'FAST', '100', 'kdniao', '0');
INSERT INTO `__PREFIX__delivery_express` VALUES ('34', 'FEDEX联邦(国内件）', 'FEDEX', '100', 'kdniao', '0');
INSERT INTO `__PREFIX__delivery_express` VALUES ('35', 'FEDEX联邦(国际件）', 'FEDEX_GJ', '100', 'kdniao', '0');
INSERT INTO `__PREFIX__delivery_express` VALUES ('36', '飞康达', 'FKD', '100', 'kdniao', '0');
INSERT INTO `__PREFIX__delivery_express` VALUES ('37', '广东邮政', 'GDEMS', '100', 'kdniao', '0');
INSERT INTO `__PREFIX__delivery_express` VALUES ('38', '共速达', 'GSD', '100', 'kdniao', '0');
INSERT INTO `__PREFIX__delivery_express` VALUES ('39', '高铁速递', 'GTSD', '100', 'kdniao', '0');
INSERT INTO `__PREFIX__delivery_express` VALUES ('40', '恒路物流', 'HLWL', '100', 'kdniao', '0');
INSERT INTO `__PREFIX__delivery_express` VALUES ('41', '天地华宇', 'HOAU', '100', 'kdniao', '0');
INSERT INTO `__PREFIX__delivery_express` VALUES ('42', '华强物流', 'hq568', '100', 'kdniao', '0');
INSERT INTO `__PREFIX__delivery_express` VALUES ('43', '华夏龙物流', 'HXLWL', '100', 'kdniao', '0');
INSERT INTO `__PREFIX__delivery_express` VALUES ('44', '好来运快递', 'HYLSD', '100', 'kdniao', '0');
INSERT INTO `__PREFIX__delivery_express` VALUES ('45', '京广速递', 'JGSD', '100', 'kdniao', '0');
INSERT INTO `__PREFIX__delivery_express` VALUES ('46', '九曳供应链', 'JIUYE', '100', 'kdniao', '0');
INSERT INTO `__PREFIX__delivery_express` VALUES ('47', '佳吉快运', 'JJKY', '100', 'kdniao', '0');
INSERT INTO `__PREFIX__delivery_express` VALUES ('48', '嘉里物流', 'JLDT', '100', 'kdniao', '0');
INSERT INTO `__PREFIX__delivery_express` VALUES ('49', '捷特快递', 'JTKD', '100', 'kdniao', '0');
INSERT INTO `__PREFIX__delivery_express` VALUES ('50', '急先达', 'JXD', '100', 'kdniao', '0');
INSERT INTO `__PREFIX__delivery_express` VALUES ('51', '晋越快递', 'JYKD', '100', 'kdniao', '0');
INSERT INTO `__PREFIX__delivery_express` VALUES ('52', '加运美', 'JYM', '100', 'kdniao', '0');
INSERT INTO `__PREFIX__delivery_express` VALUES ('53', '佳怡物流', 'JYWL', '100', 'kdniao', '0');
INSERT INTO `__PREFIX__delivery_express` VALUES ('54', '跨越物流', 'KYWL', '100', 'kdniao', '0');
INSERT INTO `__PREFIX__delivery_express` VALUES ('55', '龙邦快递', 'LB', '100', 'kdniao', '0');
INSERT INTO `__PREFIX__delivery_express` VALUES ('56', '联昊通速递', 'LHT', '100', 'kdniao', '0');
INSERT INTO `__PREFIX__delivery_express` VALUES ('57', '民航快递', 'MHKD', '100', 'kdniao', '0');
INSERT INTO `__PREFIX__delivery_express` VALUES ('58', '明亮物流', 'MLWL', '100', 'kdniao', '0');
INSERT INTO `__PREFIX__delivery_express` VALUES ('59', '能达速递', 'NEDA', '100', 'kdniao', '0');
INSERT INTO `__PREFIX__delivery_express` VALUES ('60', '平安达腾飞快递', 'PADTF', '100', 'kdniao', '0');
INSERT INTO `__PREFIX__delivery_express` VALUES ('61', '全晨快递', 'QCKD', '100', 'kdniao', '0');
INSERT INTO `__PREFIX__delivery_express` VALUES ('62', '全日通快递', 'QRT', '100', 'kdniao', '0');
INSERT INTO `__PREFIX__delivery_express` VALUES ('63', '如风达', 'RFD', '100', 'kdniao', '0');
INSERT INTO `__PREFIX__delivery_express` VALUES ('64', '赛澳递', 'SAD', '100', 'kdniao', '0');
INSERT INTO `__PREFIX__delivery_express` VALUES ('65', '圣安物流', 'SAWL', '100', 'kdniao', '0');
INSERT INTO `__PREFIX__delivery_express` VALUES ('66', '盛邦物流', 'SBWL', '100', 'kdniao', '0');
INSERT INTO `__PREFIX__delivery_express` VALUES ('67', '上大物流', 'SDWL', '100', 'kdniao', '0');
INSERT INTO `__PREFIX__delivery_express` VALUES ('68', '盛丰物流', 'SFWL', '100', 'kdniao', '0');
INSERT INTO `__PREFIX__delivery_express` VALUES ('69', '盛辉物流', 'SHWL', '100', 'kdniao', '0');
INSERT INTO `__PREFIX__delivery_express` VALUES ('70', '速通物流', 'ST', '100', 'kdniao', '0');
INSERT INTO `__PREFIX__delivery_express` VALUES ('71', '速腾快递', 'STWL', '100', 'kdniao', '0');
INSERT INTO `__PREFIX__delivery_express` VALUES ('72', '速尔快递', 'SURE', '100', 'kdniao', '0');
INSERT INTO `__PREFIX__delivery_express` VALUES ('73', '唐山申通', 'TSSTO', '100', 'kdniao', '0');
INSERT INTO `__PREFIX__delivery_express` VALUES ('74', '全一快递', 'UAPEX', '100', 'kdniao', '0');
INSERT INTO `__PREFIX__delivery_express` VALUES ('75', '万家物流', 'WJWL', '100', 'kdniao', '0');
INSERT INTO `__PREFIX__delivery_express` VALUES ('76', '万象物流', 'WXWL', '100', 'kdniao', '0');
INSERT INTO `__PREFIX__delivery_express` VALUES ('77', '新邦物流', 'XBWL', '100', 'kdniao', '0');
INSERT INTO `__PREFIX__delivery_express` VALUES ('78', '信丰快递', 'XFEX', '100', 'kdniao', '0');
INSERT INTO `__PREFIX__delivery_express` VALUES ('79', '希优特', 'XYT', '100', 'kdniao', '0');
INSERT INTO `__PREFIX__delivery_express` VALUES ('80', '新杰物流', 'XJ', '100', 'kdniao', '0');
INSERT INTO `__PREFIX__delivery_express` VALUES ('81', '源安达快递', 'YADEX', '100', 'kdniao', '0');
INSERT INTO `__PREFIX__delivery_express` VALUES ('82', '远成物流', 'YCWL', '100', 'kdniao', '0');
INSERT INTO `__PREFIX__delivery_express` VALUES ('83', '义达国际物流', 'YDH', '100', 'kdniao', '0');
INSERT INTO `__PREFIX__delivery_express` VALUES ('84', '越丰物流', 'YFEX', '100', 'kdniao', '0');
INSERT INTO `__PREFIX__delivery_express` VALUES ('85', '原飞航物流', 'YFHEX', '100', 'kdniao', '0');
INSERT INTO `__PREFIX__delivery_express` VALUES ('86', '亚风快递', 'YFSD', '100', 'kdniao', '0');
INSERT INTO `__PREFIX__delivery_express` VALUES ('87', '运通快递', 'YTKD', '100', 'kdniao', '0');
INSERT INTO `__PREFIX__delivery_express` VALUES ('88', '亿翔快递', 'YXKD', '100', 'kdniao', '0');
INSERT INTO `__PREFIX__delivery_express` VALUES ('89', '增益快递', 'ZENY', '100', 'kdniao', '0');
INSERT INTO `__PREFIX__delivery_express` VALUES ('90', '汇强快递', 'ZHQKD', '100', 'kdniao', '0');
INSERT INTO `__PREFIX__delivery_express` VALUES ('91', '众通快递', 'ZTE', '100', 'kdniao', '0');
INSERT INTO `__PREFIX__delivery_express` VALUES ('92', '中邮物流', 'ZYWL', '100', 'kdniao', '0');
INSERT INTO `__PREFIX__delivery_express` VALUES ('93', '速必达物流', 'SUBIDA', '100', 'kdniao', '0');
INSERT INTO `__PREFIX__delivery_express` VALUES ('94', '瑞丰速递', 'RFEX', '100', 'kdniao', '0');
INSERT INTO `__PREFIX__delivery_express` VALUES ('95', '快客快递', 'QUICK', '100', 'kdniao', '0');
INSERT INTO `__PREFIX__delivery_express` VALUES ('96', 'CNPEX中邮快递', 'CNPEX', '100', 'kdniao', '0');
INSERT INTO `__PREFIX__delivery_express` VALUES ('97', '鸿桥供应链', 'HOTSCM', '100', 'kdniao', '0');
INSERT INTO `__PREFIX__delivery_express` VALUES ('98', '海派通物流公司', 'HPTEX', '100', 'kdniao', '0');
INSERT INTO `__PREFIX__delivery_express` VALUES ('99', '澳邮专线', 'AYCA', '100', 'kdniao', '0');
INSERT INTO `__PREFIX__delivery_express` VALUES ('100', '泛捷快递', 'PANEX', '100', 'kdniao', '0');
INSERT INTO `__PREFIX__delivery_express` VALUES ('101', 'PCA Express', 'PCA', '100', 'kdniao', '0');
INSERT INTO `__PREFIX__delivery_express` VALUES ('102', 'UEQ Express', 'UEQ', '100', 'kdniao', '0');






