<?php

namespace addons\delivery\model;

use think\Model;
use traits\model\SoftDelete;
/**
 * 标签模型
 */
class ExpressBillSender Extends Model
{
    use SoftDelete;
    protected $name = "delivery_express_bill_sender";
    // 开启自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';
    // 定义时间戳字段名
    protected $createTime = 'addtime';
    protected $updateTime = '';
    protected $deleteTime = 'deletetime';
    // 追加属性
    protected $append = [

    ];
    protected static $config = [];
    public function getStatusList()
    {
        return ['1' => __('按重计费'), '2' => __('按件计费')];
    }


}
