<?php

namespace addons\delivery\model;

use app\models\MchPostageRules;
use think\Model;
use traits\model\SoftDelete;
/**
 * 标签模型
 */
class PostageRules Extends Model
{
    use SoftDelete;
    protected $name = "delivery_postage_rules";
    // 开启自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';
    // 定义时间戳字段名
    protected $createTime = 'addtime';
    protected $updateTime = '';
    protected $deleteTime = 'deletetime';
    // 追加属性
    protected $append = [

    ];
    protected static $config = [];
    public function getStatusList()
    {
        return ['1' => __('按重计费'), '2' => __('按件计费')];
    }

    /**
     * 计算运费
     * @param $province_id
     * @param $goods
     * @return float|int
     */
    public static function getExpressPriceMore( $city_id, $goodsList, $province_id)
    {

        $newGoodsList = [];
        foreach ($goodsList as $row) {
            if (isset($newGoodsList[$row['freight']])) {
                $newGoodsList[$row['freight']]['num'] += $row['num'];
                $newGoodsList[$row['freight']]['weight'] += $row['weight'];
            } else {
                $newGoodsList[$row['freight']] = $row;
            }
        }

        foreach ($newGoodsList as $key => $goods) {
            if ($goods['freight'] != '0') {
                $postage_rules = self::get($goods['freight']);
            } else {
                $postage_rules = self::get(['is_default'=>1]);
            }

            if ($postage_rules) {
                $list = json_decode($postage_rules['detail'],true);

                $matching = null;
                foreach ($list as $i => $item) {
                    $in_array = false;
                    if(in_array($province_id,$item['province']) || in_array($city_id,$item['province'])){
                        $in_array = true;
                    }
                    if ($in_array) {
                        $matching = $list[$i];
                    }
                }
                $newGoodsList[$key]['type']       = $postage_rules['type'];
                $newGoodsList[$key]['matching']   = $matching;
            }
        }

        $maxFristPrice = 0;
        $maxFristPriceIndex = null;

        foreach ($newGoodsList as $k => $m) {
            if(isset($m['matching']['frist_price'])){
                if ($m['matching']['frist_price'] >= $maxFristPrice) {
                    $maxFristPrice = $m['matching']['frist_price'] ;
                    $maxFristPriceIndex = $k;
                }
            }

        }

        $price = 0.00;
        foreach ($newGoodsList as $key => $value) {
            if ($key == $maxFristPriceIndex) {
                if ($value['type'] == '1') {
                    // 按重计费
                    $totalWeight = $value['weight'];
                    $totalWeight -= $value['matching']['frist'];
                    $price += $value['matching']['frist_price'];
                    if ($value['matching']['second']) {
                        $leave = ceil($totalWeight / $value['matching']['second']) > 0 ? ceil($totalWeight / $value['matching']['second']) : 0;
                    } else {
                        $leave = 0;
                    }
                    $price += $leave * $value['matching']['second_price'];
                } else {
                    // 按件计费
                    $value['num'] -= $value['matching']['frist'];
                    $price += $value['matching']['frist_price'];
                    if ($value['matching']['second']) {
                        $leave = ceil($value['num'] / $value['matching']['second']) > 0 ? ceil($value['num'] / $value['matching']['second']) : 0;
                    } else {
                        $leave = 0;
                    }
                    $price += $leave * $value['matching']['second_price'];
                }
            } else {
                if ($value['type'] == '1') {
                    // 按重计费
                    $totalWeight = $value['weight'];
                    if ($value['matching']['second']) {
                        $leave = ceil($totalWeight / $value['matching']['second']) > 0 ? ceil($totalWeight / $value['matching']['second']) : 0;
                    } else {
                        $leave = 0;
                    }
                    $price += $leave * $value['matching']['second_price'];
                } else {
                    // 按件计费
                    if ($value['matching']['second']) {
                        $leave = ceil($value['num'] / $value['matching']['second']) > 0 ? ceil($value['num'] / $value['matching']['second']) : 0;
                    } else {
                        $leave = 0;
                    }
                    $price += $leave * $value['matching']['second_price'];
                }
            }
        }
        return $price;
    }
}
