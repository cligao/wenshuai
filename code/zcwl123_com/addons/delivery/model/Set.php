<?php

namespace addons\delivery\model;

use think\Model;
/**
 * 标签模型
 */
class Set Extends Model
{
    protected $name = "delivery_set";
    // 开启自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';
    // 定义时间戳字段名
    protected $createTime = '';
    protected $updateTime = '';
    protected $deleteTime = '';
    // 追加属性
    protected $append = [

    ];
    protected static $config = [];
}
