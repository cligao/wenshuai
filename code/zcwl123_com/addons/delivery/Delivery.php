<?php

namespace addons\delivery;

use app\common\library\Menu;
use think\Addons;

/**
 * 插件
 */
class Delivery extends Addons
{

    /**
     * 插件安装方法
     * @return bool
     */
    public function install()
    {
        $menu = [
            [
                'name'    => 'delivery',
                'title'   => '发货管理',
                'icon'    => 'fa fa-send',
                'sublist' => [
                    [
                        'name'    => 'delivery/goods',
                        'title'   => '商品管理',
                        'icon'    => 'fa fa-tag',
                        'sublist' => [
                            ['name' => 'delivery/goods/index', 'title' => '查看'],
                            ['name' => 'delivery/goods/add', 'title' => '添加'],
                            ['name' => 'delivery/goods/edit', 'title' => '修改'],
                            ['name' => 'delivery/goods/del', 'title' => '删除'],
                            ['name' => 'delivery/goods/multi', 'title' => '批量更新'],
                            ['name' => 'delivery/goods/addcat', 'title' => '添加商品分类'],
                        ]
                    ],
                    [
                        'name'    => 'delivery/order',
                        'title'   => '发货订单',
                        'icon'    => 'fa fa-list',
                        'sublist' => [
                            ['name' => 'delivery/order/index', 'title' => '查看'],
                            ['name' => 'delivery/order/add', 'title' => '添加'],
                            ['name' => 'delivery/order/delivery', 'title' => '发货'],
                            ['name' => 'delivery/order/del', 'title' => '删除'],
                            ['name' => 'delivery/order/multi', 'title' => '批量更新'],
                            ['name' => 'delivery/order/detail', 'title' => '发货单详情'],
                        ]
                    ],
                    [
                        'name'    => 'delivery/postage',
                        'title'   => '运费规则',
                        'icon'    => 'fa fa-truck',
                        'sublist' => [
                            ['name' => 'delivery/postage/index', 'title' => '查看'],
                            ['name' => 'delivery/postage/add', 'title' => '添加'],
                            ['name' => 'delivery/postage/edit', 'title' => '修改'],
                            ['name' => 'delivery/postage/del', 'title' => '删除'],
                            ['name' => 'delivery/postage/multi', 'title' => '批量更新'],
                            ['name' => 'delivery/postage/select', 'title' => '运费规则'],
                        ]
                    ],
                    [
                        'name'    => 'delivery/freedeliveryrules',
                        'title'   => '包邮规则',
                        'icon'    => 'fa fa-bookmark',
                        'sublist' => [
                            ['name' => 'delivery/freedeliveryrules/index', 'title' => '查看'],
                            ['name' => 'delivery/freedeliveryrules/add', 'title' => '添加'],
                            ['name' => 'delivery/freedeliveryrules/edit', 'title' => '修改'],
                            ['name' => 'delivery/freedeliveryrules/del', 'title' => '删除'],
                            ['name' => 'delivery/freedeliveryrules/multi', 'title' => '批量更新'],
                            ['name' => 'delivery/freedeliveryrules/select', 'title' => '包邮规则'],
                        ]
                    ],
                    [
                        'name'    => 'delivery/expressbillprint',
                        'title'   => '快递单打印',
                        'icon'    => 'fa fa-barcode',
                        'sublist' => [
                            ['name' => 'delivery/expressbillprint/index', 'title' => '查看'],
                            ['name' => 'delivery/expressbillprint/add', 'title' => '添加'],
                            ['name' => 'delivery/expressbillprint/edit', 'title' => '修改'],
                            ['name' => 'delivery/expressbillprint/del', 'title' => '删除'],
                            ['name' => 'delivery/expressbillprint/multi', 'title' => '批量更新'],
                            ['name' => 'delivery/expressbillprint/defaultsender', 'title' => '默认发件人'],
                        ]
                    ],
                ]
            ]
        ];
        Menu::create($menu);
        return true;
    }

    /**
     * 插件卸载方法
     * @return bool
     */
    public function uninstall()
    {
        Menu::delete('delivery');
        return true;
    }

    /**
     * 插件启用方法
     * @return bool
     */
    public function enable()
    {
        Menu::enable("delivery");
        return true;
    }

    /**
     * 插件禁用方法
     * @return bool
     */
    public function disable()
    {
        Menu::disable("delivery");
        return true;
    }



}
