<?php

return [
    'Ciid'                         => '排课表',
    'Subject_id'                   => '课目id',
    'Classroom'                    => '教室号',
    'Days'                         => '周几',
    'Nums'                         => '第几节课开始',
    'Duration'                     => '大小课,45分钟的倍数',
    'Attend'                       => '上课周数',
    'Ctype'                        => '课程类型（0代表普通课,1代表实验课,2代表网课,3代表考试,4代表其他）',
    'Tname'                        => '老师姓名',
    'Timetablesubjects.subject_id' => '课目id',
    'Timetablesubjects.status'     => '科目类型,0表示线下课,1表示线上课,2表示实践课',
    'Timetablesubjects.sname'      => '课目名称',
    'Timetablesubjects.credit'     => '学分'
];
