<?php

return [
    'Subject_id' => '课目id',
    'Status'     => '科目类型',
    'Sname'      => '课目名称',
    'Credit'     => '学分',
    'Status 0'   => '线下课',
    'Status 1'   => '线上课',
    'Status 2'   => '实践课'
];
