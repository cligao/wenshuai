<?php

namespace app\admin\model\timetable;

use think\Model;


class Lovers extends Model
{

    

    

    // 表名
    protected $name = 'timetable_lovers';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = false;

    // 定义时间戳字段名
    protected $createTime = false;
    protected $updateTime = false;
    protected $deleteTime = false;

    // 追加属性
    protected $append = [
        'starttime_text',
        'endtime_text',
        'love_sort_text'
    ];
    

    
    public function getLoveSortList()
    {
        return ['0' => __('Love_sort 0'), '1' => __('Love_sort 1'), '2' => __('Love_sort 2')];
    }


    public function getStarttimeTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['starttime']) ? $data['starttime'] : '');
        return is_numeric($value) ? date("Y-m-d H:i:s", $value) : $value;
    }


    public function getEndtimeTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['endtime']) ? $data['endtime'] : '');
        return is_numeric($value) ? date("Y-m-d H:i:s", $value) : $value;
    }


    public function getLoveSortTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['love_sort']) ? $data['love_sort'] : '');
        $list = $this->getLoveSortList();
        return isset($list[$value]) ? $list[$value] : '';
    }

    protected function setStarttimeAttr($value)
    {
        return $value === '' ? null : ($value && !is_numeric($value) ? strtotime($value) : $value);
    }

    protected function setEndtimeAttr($value)
    {
        return $value === '' ? null : ($value && !is_numeric($value) ? strtotime($value) : $value);
    }

    public function userset()
    {
        return $this->belongsTo('app\admin\model\timetable\Usersetting', 'fid', 'user_id', [], 'LEFT')->setEagerlyType(0);
    }

    public function fromuser()
    {
        return $this->belongsTo('app\admin\model\User', 'fid', 'id', [], 'LEFT')->setEagerlyType(0);
    }


    public function touser()
    {
        return $this->belongsTo('app\admin\model\User', 'tid', 'id', [], 'LEFT')->setEagerlyType(0);
    }
}
