<?php

namespace app\admin\model\timetable;

use think\Model;


class Usersetting extends Model
{

    

    

    // 表名
    protected $name = 'timetable_usersetting';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = false;

    // 定义时间戳字段名
    protected $createTime = false;
    protected $updateTime = false;
    protected $deleteTime = false;

    // 追加属性
    protected $append = [
        'has_apply_text',
        'id_show_lover_text'
    ];
    

    
    public function getHasApplyList()
    {
        return ['0' => __('Has_apply 0'), '1' => __('Has_apply 1')];
    }

    public function getIdShowLoverList()
    {
        return ['0' => __('Id_show_lover 0'), '1' => __('Id_show_lover 1')];
    }


    public function getHasApplyTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['has_apply']) ? $data['has_apply'] : '');
        $list = $this->getHasApplyList();
        return isset($list[$value]) ? $list[$value] : '';
    }


    public function getIdShowLoverTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['id_show_lover']) ? $data['id_show_lover'] : '');
        $list = $this->getIdShowLoverList();
        return isset($list[$value]) ? $list[$value] : '';
    }

    public static function setUserSetting($uid, $schoolid){
        $user_info = Usersetting::where('user_id', $uid)->find();
        if(!$user_info){
            $row = new Usersetting();
            $data['user_id'] = $uid;
            $row->create($data);
        }
    }


    public function user()
    {
        return $this->belongsTo('app\admin\model\User', 'user_id', 'id', [], 'LEFT')->setEagerlyType(0)->bind('nickname,avatar,gender');
    }
}
