<?php

namespace app\admin\model\timetable;

use think\Model;


class Usercourse extends Model
{

    

    

    // 表名
    protected $name = 'timetable_usercourse';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = false;

    // 定义时间戳字段名
    protected $createTime = false;
    protected $updateTime = false;
    protected $deleteTime = false;

    // 追加属性
    protected $append = [

    ];
    

    







    public function user()
    {
        return $this->belongsTo('app\admin\model\User', 'user_id', 'id', [], 'LEFT')->setEagerlyType(0);
    }


    public function timetablecourse()
    {
        return $this->belongsTo('app\admin\model\timetable\Course', 'ciid', 'ciid', [], 'LEFT')->setEagerlyType(0)->bind('subject_id,classroom,days,nums,enum,attend,ctype,tname');
    }
}
