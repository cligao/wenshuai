<?php

return array (
  'name' => '众创云集',
  'beian' => '',
  'cdnurl' => '',
  'version' => '1.0.19',
  'timezone' => 'Asia/Shanghai',
  'forbiddenip' => '',
  'languages' => 
  array (
    'backend' => 'zh-cn',
    'frontend' => 'zh-cn',
  ),
  'fixedpage' => 'dashboard',
  'categorytype' => 
  array (
    'default' => '默认',
    'page' => '单页',
    'article' => '文章',
    'test' => 'Test',
  ),
  'configgroup' => 
  array (
    'basic' => '基础配置',
    'email' => '邮件配置',
    'dictionary' => '字典配置',
    'user' => '会员配置',
    'example' => '示例分组',
  ),
  'mail_type' => '1',
  'mail_smtp_host' => 'smtp.qq.com',
  'mail_smtp_port' => '465',
  'mail_smtp_user' => '10000',
  'mail_smtp_pass' => 'password',
  'mail_verify_type' => '2',
  'mail_from' => '10000@qq.com',
  'attachmentcategory' => 
  array (
    'shouye' => '首页图片',
    'course' => '课程图片',
    'allcategories' => '全部分类图标',
  ),
  'zcjl' => '5',
  'fxjl' => '2.5',
);
